open module Main.app {

    requires javafx.fxml;
    requires javafx.controls;
    requires org.jetbrains.annotations;
    requires org.slf4j;
}
