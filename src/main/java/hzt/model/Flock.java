package hzt.model;

import hzt.model.boid3d.Boid3D;
import hzt.model.boid_properties.FlockProperties;
import hzt.model.component3d.Line3D;
import hzt.model.utils.Engines;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import org.jetbrains.annotations.NotNull;

import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.stream.Stream;

import static hzt.model.AppConstants.INIT_SELECTED_BOID_COLOR;
import static hzt.model.Aquarium.getRandomPositionInAquarium;

public class Flock extends Group implements Iterable<Boid3D> {

    private final FlockProperties flockProperties = new FlockProperties();
    private final Scene mainScene;

    private final ObjectProperty<Boid3D> selectedBoid = new SimpleObjectProperty<>();
    private final ObjectProperty<Color> selectedBoidColor = new SimpleObjectProperty<>();
    private final ObjectProperty<FlockType> flockType = new SimpleObjectProperty<>();
    private final ObjectProperty<Engines.FlockingSim> flockingSim = new SimpleObjectProperty<>();

    public Flock(Scene mainScene) {
        this.mainScene = mainScene;
        this.selectedBoidColor.set(INIT_SELECTED_BOID_COLOR);
    }

    public void setFlockSize(Number number) {
        setFlockSize(number.intValue());
    }

    public void setFlockSize(int wantedNumberOfBoids) {
        long numberOfBoids = stream().count();
        while (numberOfBoids != wantedNumberOfBoids) {
            if (numberOfBoids < wantedNumberOfBoids) {
                add3DBoidToFlock();
            } else {
                removeBoidFromFlock();
            }
            numberOfBoids = stream().count();
        }
    }

    private void add3DBoidToFlock() {
        Boid3D boid = flockType.get().createBoid(flockProperties.getMaxBallSize(), flockProperties.getBoidColor());
        boid.setBodyTranslate(getRandomPositionInAquarium());
        super.getChildren().add(boid);
        boid.setBoidParams(flockProperties);
        boid.addBoidComponents(super.getChildren());
        boid.addMouseFunctionality();
    }

    private void removeBoidFromFlock() {
        Boid3D boid = unmodifiableBoidsList().get(0);
        boid.removeBoidComponents(super.getChildren());
        super.getChildren().remove(boid);
        for (Boid3D boid3D : unmodifiableBoidsList()) {
            boid3D.getPerceptionRadiusMap().remove(boid);
            boid3D.getConnections().removeIf(Line3D.class::isInstance);
        }
        if (boid.equals(selectedBoid.get())) {
            selectedBoid.set(!unmodifiableBoidsList().isEmpty() ? getRandomBoidFromFlock() : null);
        }
    }

    public Boid3D getRandomBoidFromFlock() {
        Boid3D selected = unmodifiableBoidsList().get(new Random().nextInt(size()));
        selected.updateColor(selectedBoidColor.get());
        selected.addKeyControlForAcceleration();
        selected.getPerceptionSphere().setVisible(flockProperties.isPerceptionVisibleSelected());
        return selected;
    }

    public void updateSelectedBoidComponentsVisibility() {
        selectedBoid.get().getPerceptionSphere().setVisible(flockProperties.isPerceptionVisibleSelected());
    }

    @NotNull
    @Override
    public Iterator<Boid3D> iterator() {
        return stream().iterator();
    }

    private List<Boid3D> unmodifiableBoidsList() {
        return stream().toList();
    }

    public Stream<Boid3D> stream() {
        return getChildren().stream()
                .filter(Boid3D.class::isInstance)
                .map(Boid3D.class::cast);
    }

    @Override
    public ObservableList<Node> getChildren() {
        return FXCollections.unmodifiableObservableList(super.getChildren());
    }

    public int size() {
        return unmodifiableBoidsList().size();
    }

    public void setPerceptionRadiusForEachBoid(Number newVal) {
        forEach(boid3D -> boid3D.setPerceptionRadiusByRatio(newVal.doubleValue()));
    }

    public void setRepelRadiusRatio(Number newVal) {
        forEach(boid3D -> boid3D.setRepelRadiusByRatio(newVal.doubleValue()));
    }

    public FlockProperties getFlockProperties() {
        return flockProperties;
    }

    public Scene getMainScene() {
        return mainScene;
    }

    public Boid3D getSelectedBoid() {
        return selectedBoid.get();
    }

    public boolean equalsSelectedBoid(Boid3D boid3D) {
        return boid3D.equals(selectedBoid.get());
    }

    public void setFlockType(FlockType flockType) {
        this.flockType.set(flockType);
    }

    public void setFlockingSim(Engines.FlockingSim flockingSim) {
        this.flockingSim.set(flockingSim);
    }

    public void setSelectedBoid(Boid3D selectedBoid) {
        this.selectedBoid.set(selectedBoid);
    }

    public Color getSelectedBoidColor() {
        return selectedBoidColor.get();
    }

    public void setSelectedBoidColor(Color selectedBoidColor) {
        this.selectedBoidColor.set(selectedBoidColor);
    }

    public FlockType getFlockType() {
        return flockType.get();
    }

    public Engines.FlockingSim getFlockingSim() {
        return flockingSim.get();
    }
}
