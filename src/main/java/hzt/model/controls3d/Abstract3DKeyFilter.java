package hzt.model.controls3d;

import javafx.event.EventHandler;
import javafx.geometry.Point3D;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public abstract class Abstract3DKeyFilter {

    private static final double DEFAULT_USER_INPUT_SIZE = .1;
    private static final Point3D X_POS_DIR = new Point3D(1, 0, 0);
    private static final Point3D Y_POS_DIR = new Point3D(0, 1, 0);
    private static final Point3D Z_POS_DIR = new Point3D(0, 0, 1);

    private final KeyCode negXKeyCode;
    private final KeyCode posXKeycode;
    private final KeyCode negYKeyCode;
    private final KeyCode posYKeyCode;
    private final KeyCode negZKeyCode;
    private final KeyCode posZKeyCode;

    private boolean xNegPressed;
    private boolean xPosPressed;
    private boolean yNegPressed;
    private boolean yPosPressed;
    private boolean zNegPressed;
    private boolean zPosPressed;

    private double userInputSize = DEFAULT_USER_INPUT_SIZE;

    private final EventHandler<KeyEvent> keyPressed;
    private final EventHandler<KeyEvent> keyReleased;

    Abstract3DKeyFilter(
            KeyCode negXKeyCode, KeyCode posXKeycode,
            KeyCode negYKeyCode, KeyCode posYKeyCode,
            KeyCode negZKeyCode, KeyCode posZKeyCode) {
        this.keyPressed = this::keyPressedAction;
        this.keyReleased = this::keyReleasedAction;
        this.negXKeyCode = negXKeyCode;
        this.posXKeycode = posXKeycode;
        this.negYKeyCode = negYKeyCode;
        this.posYKeyCode = posYKeyCode;
        this.negZKeyCode = negZKeyCode;
        this.posZKeyCode = posZKeyCode;
    }

    private static boolean keyPressed(KeyEvent enteredKey, KeyCode comparedKey, boolean isPressed) {
        return enteredKey.getCode() == comparedKey && !isPressed;
    }

    abstract boolean pressedAction(Point3D point3D);

    abstract boolean releasedAction(Point3D point3D);

    abstract void allReleasedAction(boolean allReleased);

    public void resetKeyPressed() {
        xNegPressed = xPosPressed = yNegPressed = yPosPressed = zNegPressed = zPosPressed = false;
    }

    public double getUserInputSize() {
        return userInputSize;
    }

    public void setUserInputSize(double userInputSize) {
        this.userInputSize = userInputSize;
    }

    public EventHandler<KeyEvent> getKeyPressed() {
        return keyPressed;
    }

    public EventHandler<KeyEvent> getKeyReleased() {
        return keyReleased;
    }

    private void keyReleasedAction(KeyEvent e) {
        if (e.getCode() == posXKeycode) {
            xPosPressed = releasedAction(X_POS_DIR.multiply(userInputSize));
        }
        if (e.getCode() == negXKeyCode) {
            xNegPressed = releasedAction(X_POS_DIR.multiply(-userInputSize));
        }
        if (e.getCode() == posYKeyCode) {
            yPosPressed = releasedAction(Y_POS_DIR.multiply(userInputSize));
        }
        if (e.getCode() == negYKeyCode) {
            yNegPressed = releasedAction(Y_POS_DIR.multiply(-userInputSize));
        }
        if (e.getCode() == posZKeyCode) {
            zPosPressed = releasedAction(Z_POS_DIR.multiply(userInputSize));
        }
        if (e.getCode() == negZKeyCode) {
            zNegPressed = releasedAction(Z_POS_DIR.multiply(-userInputSize));
        }
        var xPressed = xNegPressed || xPosPressed;
        var yPressed = yNegPressed || yPosPressed;
        var zPressed = zNegPressed || zPosPressed;
        boolean allReleased = !(xPressed || yPressed || zPressed);
        allReleasedAction(allReleased);
    }

    private void keyPressedAction(KeyEvent e) {
        if (keyPressed(e, posXKeycode, xPosPressed)) {
            xPosPressed = pressedAction(X_POS_DIR.multiply(userInputSize));
        }
        if (keyPressed(e, negXKeyCode, xNegPressed)) {
            xNegPressed = pressedAction(X_POS_DIR.multiply(-userInputSize));
        }
        if (keyPressed(e, posYKeyCode, yPosPressed)) {
            yPosPressed = pressedAction(Y_POS_DIR.multiply(userInputSize));
        }
        if (keyPressed(e, negYKeyCode, yNegPressed)) {
            yNegPressed = pressedAction(Y_POS_DIR.multiply(-userInputSize));
        }
        if (keyPressed(e, posZKeyCode, zPosPressed)) {
            zPosPressed = pressedAction(Z_POS_DIR.multiply(userInputSize));
        }
        if (keyPressed(e, negZKeyCode, zNegPressed)) {
            zNegPressed = pressedAction(Z_POS_DIR.multiply(-userInputSize));
        }
    }
}
