package hzt.model.utils;

import org.slf4j.Logger;

import java.util.function.Supplier;

public final class Loggers {

    private Loggers() {
    }

    public static void trace(Logger logger, Supplier<String> supplier) {
        if (logger.isTraceEnabled()) {
            logger.trace(supplier.get());
        }
    }

    public static void debug(Logger logger, Supplier<String> supplier) {
        if (logger.isDebugEnabled()) {
            logger.debug(supplier.get());
        }
    }

    public static void info(Logger logger, Supplier<String> supplier) {
        if (logger.isInfoEnabled()) {
            logger.info(supplier.get());
        }
    }

}
