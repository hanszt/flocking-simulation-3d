package hzt.model.utils;

import javafx.beans.value.ChangeListener;
import javafx.scene.Node;
import javafx.scene.Parent;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;

public final class FxUtils {

    private FxUtils() {
    }

    public static List<Node> allDescendents(Parent root) {
        List<Node> nodes = new ArrayList<>();
        addAllDescendents(root, nodes);
        return nodes;
    }

    private static void addAllDescendents(Parent parent, List<Node> nodes) {
        for (Node node : parent.getChildrenUnmodifiable()) {
            nodes.add(node);
            if (node instanceof Parent newParent) {
                addAllDescendents(newParent, nodes);
            }
        }
    }

    public static <N> ChangeListener<N> newValue(Consumer<N> consumer) {
        Objects.requireNonNull(consumer);
        return (o, c, newVal) -> consumer.accept(newVal);
    }

}
