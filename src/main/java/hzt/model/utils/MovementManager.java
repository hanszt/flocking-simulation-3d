package hzt.model.utils;

import javafx.geometry.Point3D;

public final class MovementManager {

    private MovementManager() {
    }

    public static Point3D limit(double maxValue, Point3D limitedVector) {
        if (limitedVector.magnitude() > maxValue) {
            limitedVector = limitedVector.normalize().multiply(maxValue);
        }
        return limitedVector;
    }

}
