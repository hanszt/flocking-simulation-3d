package hzt.model.utils;

import hzt.model.boid3d.Ball3D;
import hzt.model.boid3d.Boid3D;
import hzt.model.boid3d.Box3D;
import hzt.model.boid3d.Cylinder3D;
import hzt.model.boid3d.Duck3D;
import hzt.model.boid3d.Icosahedron3D;
import hzt.model.boid3d.Pyramid3D;
import hzt.model.boid3d.Scooter3D;
import hzt.model.boid3d.Toroid3D;
import hzt.model.custom_shapes.Toroid;
import javafx.geometry.Point3D;
import javafx.scene.paint.Color;
import javafx.scene.shape.Box;
import javafx.scene.shape.Cylinder;

import java.util.List;
import java.util.Random;
import java.util.function.DoubleFunction;

import static java.lang.Math.random;

public final class RandomGenerator {

    private static final Random RANDOM = new Random();
    private static final List<DoubleFunction<Boid3D>> RANDOM_BOID_GENERATORS = List.of(
            RandomGenerator::getRandomBall3D,
            RandomGenerator::getRandomBox3D,
            RandomGenerator::getRandomIcosahedron,
            RandomGenerator::getRandomPyramid,
            RandomGenerator::getRandomDuck,
            RandomGenerator::getRandomScooter,
            RandomGenerator::getRandomToroid,
            RandomGenerator::getRandomCylinder3D);

    private RandomGenerator() {
    }

    public static Point3D getRandomPoint3D(Point3D minValue, Point3D maxValue) {
        return new Point3D(
                getRandomDouble(minValue.getX(), maxValue.getX()),
                getRandomDouble(minValue.getY(), maxValue.getY()),
                getRandomDouble(minValue.getZ(), maxValue.getZ()));
    }

    public static Point3D getRandomAxisOfRotation() {
        return getRandomPoint3D(new Point3D(-1, -1, -1), new Point3D(1, 1, 1).normalize());
    }

    public static Boid3D getRandomBoid3D(double max) {
        return RANDOM_BOID_GENERATORS.get(RANDOM.nextInt(RANDOM_BOID_GENERATORS.size())).apply(max);
    }

    public static Ball3D getRandomBall3D(double max) {
        return new Ball3D(getRandomDouble(max), getRandomColor());
    }

    public static Box3D getRandomBox3D(double max) {
        var box = new Box3D(getRandomColor());
        Box body = (Box) box.getBody();
        body.setWidth(getRandomDouble(max));
        body.setHeight(getRandomDouble(max));
        body.setDepth(getRandomDouble(max));
        return box;
    }

    public static Cylinder3D getRandomCylinder3D(double max) {
        var body = new Cylinder();
        body.setHeight(getRandomDouble(max));
        body.setRadius(getRandomDouble(max));
        return new Cylinder3D(body, getRandomColor());
    }

    public static Icosahedron3D getRandomIcosahedron(double max) {
        return new Icosahedron3D(getRandomDouble(max), getRandomColor());
    }

    public static Pyramid3D getRandomPyramid(double max) {
        return new Pyramid3D(getRandomDouble(max), getRandomColor());
    }

    public static Toroid3D getRandomToroid(double max) {
        final var ringRadius = getRandomDouble(max);
        final var MIN = ringRadius / 10;
        final var MAX = ringRadius / 2;
        final var tubeRadius = getRandomDouble(MIN, MAX);
        final var toroid = new Toroid(ringRadius, tubeRadius);
        return new Toroid3D(toroid, getRandomColor());
    }

    public static Duck3D getRandomDuck(double max) {
        return new Duck3D(getRandomDouble(max), getRandomColor());
    }

    public static Scooter3D getRandomScooter(double max) {
        return new Scooter3D(getRandomDouble(max), getRandomColor());
    }

    private static Color getRandomColor() {
        return Color.color(random(), random(), random());
    }

    private static double getRandomDouble(double max) {
        return getRandomDouble(0, max);
    }

    public static double getRandomDouble(double min, double max) {
        return random() * (max - min) + min;
    }
}
