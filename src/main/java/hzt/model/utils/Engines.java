package hzt.model.utils;

import hzt.model.boid3d.Boid3D;
import javafx.beans.property.FloatProperty;
import javafx.beans.property.SimpleFloatProperty;
import javafx.geometry.Point3D;

public class Engines {

    private final FloatProperty pullFactor = new SimpleFloatProperty();
    private final FloatProperty repelFactor = new SimpleFloatProperty();

    /*
     * Fg = (G * m_self * m_other) / r^2
     * F_res = m_self * a ->
     * <p>
     * Fg = F_res
     * <p>
     * a = (G * m_other) / r^2
     */
    public abstract static class FlockingSim {

        public Point3D getTotalAcceleration(Boid3D self, Iterable<Boid3D> boid3DSet) {
            Point3D totalAcceleration = Point3D.ZERO;
            for (Boid3D other : boid3DSet) {
                Point3D acceleration = getAccelerationBetweenTwoBoids(self, other);
                totalAcceleration = totalAcceleration.add(acceleration);
            }
            return totalAcceleration;
        }

        abstract Point3D getAccelerationBetweenTwoBoids(Boid3D self, Boid3D other);

        @Override
        public abstract String toString();
    }

    private final FlockingSim type1 = new FlockingSim() {

        Point3D getAccelerationBetweenTwoBoids(Boid3D self, Boid3D other) {
            Point3D vectorSelfToOther = other.getBodyTranslate().subtract(self.getBodyTranslate());
            Point3D unitVectorInAccDir = vectorSelfToOther.normalize();
            float distance = (float) vectorSelfToOther.magnitude();
            float part2Formula = (float) other.getMass() / (distance * distance);
            float attractionMagnitude = pullFactor.get() * part2Formula;
            float repelMagnitude = repelFactor.get() * part2Formula;
            float repelDistance = self.getRepelRadius() + other.getRepelRadius();

            return distance <= repelDistance ?
                    unitVectorInAccDir.multiply(-repelMagnitude) : unitVectorInAccDir.multiply(attractionMagnitude);
        }

        @Override
        public String toString() {
            return "Engine type 1";
        }
    };

    private final FlockingSim type2 = new FlockingSim() {

        Point3D getAccelerationBetweenTwoBoids(Boid3D self, Boid3D other) {
            Point3D vectorSelfToOther = other.getBodyTranslate().subtract(self.getBodyTranslate());
            Point3D unitVectorInAccDir = vectorSelfToOther.normalize();
            float distance = (float) vectorSelfToOther.magnitude();
            float part2Formula = (float) other.getMass() / (distance * distance);
            float repelDistance = self.getRepelRadius() + other.getRepelRadius();
            float curveFitConstant = (float) (other.getMass() * (pullFactor.get() + repelFactor.get()) /
                    (repelDistance * repelDistance));
            float attractionMagnitude = pullFactor.get() * part2Formula;
            float repelMagnitude = -repelFactor.get() * part2Formula + curveFitConstant;
            return distance <= repelDistance ?
                    unitVectorInAccDir.multiply(repelMagnitude) : unitVectorInAccDir.multiply(attractionMagnitude);
        }

        @Override
        public String toString() {
            return "Engine type 2";
        }
    };

    private final FlockingSim type3 = new FlockingSim() {

        Point3D getAccelerationBetweenTwoBoids(Boid3D self, Boid3D other) {
            final var MULTIPLIER = 10;
            Point3D vectorSelfToOther = other.getBodyTranslate().subtract(self.getBodyTranslate());
            Point3D unitVectorInAccDir = vectorSelfToOther.normalize();
            float distance = (float) vectorSelfToOther.magnitude();
            float repelDistance = self.getRepelRadius() + other.getRepelRadius();

            return distance <= repelDistance ?
                    unitVectorInAccDir.multiply(-repelFactor.get() * MULTIPLIER) :
                    unitVectorInAccDir.multiply(pullFactor.get() * MULTIPLIER);
        }

        @Override
        public String toString() {
            return "Engine type 3";
        }
    };

    public FloatProperty pullFactorProperty() {
        return pullFactor;
    }

    public FloatProperty repelFactorProperty() {
        return repelFactor;
    }

    public FlockingSim getType1() {
        return type1;
    }

    public FlockingSim getType2() {
        return type2;
    }

    public FlockingSim getType3() {
        return type3;
    }
}
