package hzt.model.utils;

import java.util.Locale;

public final class StringUtils {

    private StringUtils() {
    }

    public static String toOnlyFirstLetterUpperCase(String string) {
        return string.charAt(0) + string.substring(1).toLowerCase(Locale.ENGLISH);
    }
}
