package hzt.model;

import hzt.model.custom_shapes.*;
import javafx.scene.shape.Box;
import javafx.scene.shape.Cylinder;
import javafx.scene.shape.Sphere;
import org.jetbrains.annotations.NotNull;

import java.util.*;

public class Aquariums implements Iterable<Aquarium> {

    private final NavigableSet<Aquarium> aquariumTreeSet = new TreeSet<>();

    public Aquariums() {
        Aquarium boxAquarium = new Aquarium(new Box(), "Box aquarium") {

            @Override
            public void setAquariumDimension(Dimension dimension) {
                Box box = (Box) getAquariumShape();
                box.setWidth(dimension.getWidth());
                box.setHeight(dimension.getHeight());
                box.setDepth(dimension.getDepth());

            }
        };
        Aquarium sphereAquarium = new Aquarium(new Sphere(), "Sphere aquarium") {

            @Override
            public void setAquariumDimension(Dimension dimension) {
                Sphere sphere = (Sphere) getAquariumShape();
                sphere.setRadius(dimension.getRadius());
            }
        };
        Aquarium icosahedronAquarium = new Aquarium(new Icosahedron(), "Icosahedron aquarium") {
            @Override
            public void setAquariumDimension(Dimension dimension) {
                Icosahedron icosahedron = (Icosahedron) getAquariumShape();
                icosahedron.setRadius(dimension.getRadius());
            }
        };
        Aquarium duckAquarium = new Aquarium(new Duck(), "Duck aquarium") {
            @Override
            public void setAquariumDimension(Dimension dimension) {
                Duck duck = (Duck) getAquariumShape();
                duck.setScaleX(dimension.getWidth());
                duck.setScaleY(dimension.getHeight());
                duck.setScaleZ(dimension.getDepth());
            }
        };
        Aquarium scooterAquarium = new Aquarium(new Scooter(), "Scooter aquarium") {
            @Override
            public void setAquariumDimension(Dimension dimension) {
                Scooter scooter = (Scooter) getAquariumShape();
                scooter.setScaleX(dimension.getWidth());
                scooter.setScaleY(dimension.getHeight());
                scooter.setScaleZ(dimension.getDepth());
            }
        };
        Aquarium pyramidAquarium = new Aquarium(new Pyramid(), "Pyramid aquarium") {
            @Override
            public void setAquariumDimension(Dimension dimension) {
                Pyramid pyramid = (Pyramid) getAquariumShape();
                pyramid.setScaleX(dimension.getWidth());
                pyramid.setScaleY(dimension.getHeight());
                pyramid.setScaleZ(dimension.getDepth());
            }
        };
        Aquarium cylinderAquarium = new Aquarium(new Cylinder(), "Cylindrical aquarium") {

            @Override
            public void setAquariumDimension(Dimension dimension) {
                Cylinder cylinder = (Cylinder) getAquariumShape();
                cylinder.setRadius(dimension.getRadius());
                cylinder.setHeight(dimension.getHeight());
            }
        };
        Aquarium toroidAquarium = new Aquarium(new Toroid(), "Toroid aquarium") {

            @Override
            public void setAquariumDimension(Dimension dimension) {
                var toroid = (Toroid) getAquariumShape();
                toroid.setScaleX(dimension.getWidth());
                toroid.setScaleY(dimension.getHeight());
                toroid.setScaleZ(dimension.getDepth());
            }
        };
        aquariumTreeSet.addAll(Set.of(boxAquarium, cylinderAquarium, sphereAquarium, toroidAquarium,
                icosahedronAquarium, duckAquarium, scooterAquarium, pyramidAquarium));
    }

    public Aquarium getRandomAquarium() {
        Aquarium aquarium = aquariumTreeSet.first();
        int counter = 0;
        int randomPicker = new Random().nextInt(aquariumTreeSet.size());
        while (counter < randomPicker) {
            aquarium = aquariumTreeSet.higher(aquarium);
            counter++;
        }
        return aquarium;
    }

    @NotNull
    @Override
    public Iterator<Aquarium> iterator() {
        return aquariumTreeSet.iterator();
    }
}
