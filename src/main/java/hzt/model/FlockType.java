package hzt.model;

import hzt.model.boid3d.Boid3D;
import javafx.scene.paint.Color;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;

public abstract class FlockType implements Comparable<FlockType> {

    private final String name;

    protected FlockType(String name) {
        this.name = name;
    }

    protected abstract Boid3D createBoid(double maxSize, Color color);

    public boolean isRandom() {
        return name.toLowerCase().contains("random");
    }

    @Override
    public int compareTo(@NotNull FlockType o) {
        return this.name.compareTo(o.name);
    }

    @Override
    public boolean equals(Object o) {
        return this == o || (o instanceof FlockType that && Objects.equals(name, that.name));
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return name;
    }
}
