package hzt.model.appearance;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;
import java.util.Optional;

public class Resource implements Comparable<Resource> {

    private final String name;
    private final String pathToResource;

    public Resource(String name) {
        this(name, "");
    }

    public Resource(String name, String pathToResource) {
        this.name = name;
        this.pathToResource = pathToResource;
    }

    @Override
    public boolean equals(Object o) {
        return this == o || (o instanceof Resource that && Objects.equals(name, that.name));
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public int compareTo(@NotNull Resource o) {
        return name.compareTo(o.name);
    }

    public String getName() {
        return name;
    }

    public Optional<String> getPathToResource() {
        return Optional.ofNullable(pathToResource);
    }

    @Override
    public String toString() {
        return name;
    }
}
