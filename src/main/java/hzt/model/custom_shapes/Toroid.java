/*
 * Copyright (c) 2019 Peter Lager
 * <quark(a)lagers.org.uk> http:www.lagers.org.uk
 *
 * This software is provided 'as-is', without any express or implied warranty.
 * In no event will the authors be held liable for any damages arising from
 * the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it freely,
 * subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented;
 * you must not claim that you wrote the original software.
 * If you use this software in a product, an acknowledgment in the product
 * documentation would be appreciated but is not required.
 *
 * 2. Altered source versions must be plainly marked as such,
 * and must not be misrepresented as being the original software.
 *
 * 3. This notice may not be removed or altered from any source distribution.
 */
package hzt.model.custom_shapes;

import javafx.beans.binding.DoubleBinding;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.geometry.Point3D;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.CullFace;
import javafx.scene.shape.DrawMode;
import javafx.scene.shape.Mesh;
import javafx.scene.shape.MeshView;
import javafx.scene.shape.TriangleMesh;

import static java.lang.Math.*;

/**
 * Simple class to create a Toroid shape, it extends MeshView class so it
 * will work seamlessly with JavaFX.<br>
 * <p>
 * NOTE: <br>
 * I would like to thank Wayne Holder for pointing out a visual artefact
 * in the torus and correctly identifying what caused it. <br>
 * The artefact looks like a seam where the mesh edges join because it is
 * wrapped round a torus. The solution was to modify the code that defined
 * the triangle so the joined edges used 'shared' vertices, see the method
 * calcFaces() for the corrected code. <br>
 * used in this program.
 * <p>
 * The original method, calcFacesPT_OpenMesh() is kept for completeness
 * and will work just fine with an open mesh.
 *
 * @author Peter Lager (2017-19)
 */
public class Toroid extends MeshView {

    // Default values
    // default 64 x 32
    private static final int NBR_RING_SEGMENTS = 64;
    private static final int NBR_TUBE_SEGMENTS = 32;
    private static final double TWO_PI = 2 * PI;
    public static final int TWO = 2;

    // Stores the number of segments which controls the surface 'smoothness'
    private final IntegerProperty nbrRingSegments = new SimpleIntegerProperty();
    private final IntegerProperty nbrTubeSegments = new SimpleIntegerProperty();
    private final IntegerProperty nbrRingSteps = new SimpleIntegerProperty();
    private final IntegerProperty nbrTubeSteps = new SimpleIntegerProperty();
    // The dimensions of the toroid
    private final DoubleProperty ringRad = new SimpleDoubleProperty();
    private final DoubleProperty tubeRadX = new SimpleDoubleProperty();
    private final DoubleProperty tubeRadY = new SimpleDoubleProperty();
    private final DoubleProperty tubeZeroPos = new SimpleDoubleProperty(); // radians

    private final PhongMaterial material;
    private final TriangleMesh mesh = new TriangleMesh();

    /**
     * Create a default Toroid using default dimensions
     */
    public Toroid() {
        this(1);
    }

    public Toroid(double diameter) {
        this(diameter, diameter / TWO);
    }

    /**
     * Create a Toroid using the default number of segments and zero angle = 0
     */
    public Toroid(double ringRad, double tubeRad) {
        this(ringRad, tubeRad, tubeRad);
    }

    /**
     * Create a Toroid using the default number of segments and zero angle = 0
     */
    public Toroid(double ringRad, double tubeRadX, double tubeRadY) {
        this(ringRad, tubeRadX, tubeRadY, 0);
    }

    /**
     * Create a Toroid using the default number of segments.
     */
    public Toroid(double ringRad, double tubeRadX, double tubeRadY, double zeroAngle) {
        this(NBR_RING_SEGMENTS, NBR_TUBE_SEGMENTS, ringRad, tubeRadX, tubeRadY, zeroAngle);
    }

    /**
     * Create a Toroid with a user defined number of segments. Using small
     * numbers can give a very angular toroid.
     *
     * @param nbrRingSegments number of segments round the ring
     * @param nbrTubeSegments number of segments round the tube
     * @param ringRad         the radius of the ring
     * @param tubeRadX        the radius of the tube along the X axis
     * @param tubeRadY        the radius of the tube along the Y axis
     * @param zeroAngle       rotates the zero-position round the tube (degrees)
     */
    public Toroid(int nbrRingSegments, int nbrTubeSegments, double ringRad,
                  double tubeRadX, double tubeRadY, double zeroAngle) {
        this.nbrRingSegments.set(nbrRingSegments);
        this.nbrRingSteps.bind(this.nbrRingSegments.add(1));
        this.nbrTubeSegments.set(nbrTubeSegments);
        this.nbrTubeSteps.bind(this.nbrTubeSegments.add(1));
        this.ringRad.set(ringRad);
        this.tubeRadX.set(tubeRadX);
        this.tubeRadY.set(tubeRadY);
        this.tubeZeroPos.set(toRadians(zeroAngle));
        this.material = new PhongMaterial(Color.BURLYWOOD);
        addListeners();
        setMesh(buildToroidMesh());
    }

    private void addListeners() {
        nbrRingSegments.addListener((o,c, n) -> calculatePointsTexCoordsAndFaces(mesh));
        nbrTubeSegments.addListener((o, c, n) -> calculatePointsTexCoordsAndFaces(mesh));
        ringRad.addListener((o, c, n) -> calculatePointsTexCoordsAndFaces(mesh));
        tubeRadX.addListener((o, c, n) -> calculatePointsTexCoordsAndFaces(mesh));
        tubeRadY.addListener((o, c, n) -> calculatePointsTexCoordsAndFaces(mesh));
        tubeZeroPos.addListener((o, c, n) -> calculatePointsTexCoordsAndFaces(mesh));
    }

    private Mesh buildToroidMesh() {
        calculatePointsTexCoordsAndFaces(mesh);
        // Create the default texture
        setMaterial(material);
        // Start with filled shape
        setDrawMode(DrawMode.FILL);
        // Makes it easier to see the wireframe
        setCullFace(CullFace.BACK);
        return mesh;
    }

    /**
     * Set the fill colour to be used for the toroid surface. Replaces existing
     * texture/colour options.
     *
     * @param color the colour to use
     */
    public void setTexture(Color color) {
        if (color != null) {
            material.setDiffuseColor(color);
        }
    }

    /**
     * Change the image to be used to texture the surface using the current
     * number of ring and tube repeats.
     *
     * @param diffuseMap the image to be used as a texture
     * @param bumpMap the bumpMap to be used
     */
    public void setTexture(Image diffuseMap, Image bumpMap) {
        if (diffuseMap != null) {
            material.setDiffuseMap(diffuseMap);
        }
        if (bumpMap != null) {
            material.setBumpMap(bumpMap);
        }
    }

    /**
     * Change the image to be used to texture the surface using the specified
     * number of ring and tube repeats.
     *
     * @param diffuseMap     the image to be used as a texture
     * @param nbrRingRepeats number of texture repeats round the ring
     * @param nbrTubeRepeats number of texture repeats round the tube
     */
    public void setTexture(Image diffuseMap, Image bumpMap, float nbrRingRepeats, float nbrTubeRepeats) {
        setTexture(diffuseMap, bumpMap);
        if (nbrRingRepeats > 0 && nbrTubeRepeats > 0) {
            ((TriangleMesh) getMesh()).getTexCoords().setAll(calcTexturePoints(nbrRingRepeats, nbrTubeRepeats));
        }
    }

    /**
     * Calculate the vertex points based on toroid dimensions and the tube zero
     * start angle.
     */
    private void calculatePointsTexCoordsAndFaces(TriangleMesh mesh) {
        double ringDeltaAng = (TWO_PI / nbrRingSegments.get());
        double tubeDeltaAng = (TWO_PI / nbrTubeSegments.get());
        calculatePointsTexCoordsAndFaces(mesh, ringDeltaAng, tubeDeltaAng);
    }

    private void calculatePointsTexCoordsAndFaces(TriangleMesh mesh, double ringDeltaAng, double tubeDeltaAng) {
        MeshData meshData = new MeshData();
        for (int t = 0; t < nbrTubeSteps.get(); t++) {
            var point0 = calculateXYCoordinateTubeInZ0Plane(tubeDeltaAng, t);
            for (int r = 0; r < nbrRingSteps.get(); r++) {
                double angle = r * ringDeltaAng;
                meshData.calculateAndAddPoint(point0, angle);
                meshData.calculateAndAddTextureCoord(r, t);
                meshData.calculateAndAddFace(t, r);
            }
        }
        meshData.setPointsTexCoordsAndFaces(mesh);
    }

    private Point3D calculateXYCoordinateTubeInZ0Plane(double tubeDeltaAng, int t) {
        double angle0 = tubeZeroPos.get() + t * tubeDeltaAng;
        double x = ringRad.get() + tubeRadX.get() * cos(angle0);
        double y = tubeRadY.get() * sin(angle0);
        return new Point3D(x, y, 0);
    }

    private static final int TWO_PER_TEXTURE_POINT = 2;

    private class MeshData {

        private static final int NR_OF_POINTS_PER_FACE_PT = 12;
        private static final int NR_OF_DIMENSIONS = 3;

        private final float[] textureCoords = new float[nbrRingSteps.get() * nbrTubeSteps.get() *
                TWO_PER_TEXTURE_POINT];
        private final float[] points = new float[nbrRingSteps.get() * nbrTubeSteps.get() * NR_OF_DIMENSIONS];
        private final int[] faces = new int[nbrRingSegments.get() * nbrTubeSegments.get() * NR_OF_POINTS_PER_FACE_PT];

        private final float defaultDeltaU = 1F / nbrRingSegments.get();
        private final float defaultDeltaV = 1F / nbrTubeSegments.get();

        private int coordsIndex = 0;
        private int pointsIndex = 0;
        private int facesIndex = 0;

        private void calculateAndAddPoint(Point3D point0, double angle) {
            var x = point0.getX() * cos(angle);
            var y = point0.getY();
            var z = point0.getX() * sin(angle);
            points[pointsIndex++] = (float) x;
            points[pointsIndex++] = (float) y;
            points[pointsIndex++] = (float) z;
        }

        private void calculateAndAddTextureCoord(float x, float y) {
            textureCoords[coordsIndex++] = x * defaultDeltaU;
            textureCoords[coordsIndex++] = y * defaultDeltaV;
        }

        /**
         * Calculate the triangle data using vertex and texture coordinate array
         * data. This code avoids a visual artefact by using 'shared' points
         * where the mesh edges meet.
         */
        private void calculateAndAddFace(int t, int r) {
            if (t < nbrTubeSegments.get() && r < nbrRingSegments.get()) {
                int r1 = r + 1;
                int t1 = t + 1;
                Indices textureIndices = calculateIndices(t, r, t1, r1);
                r1 %= nbrRingSegments.get();
                t1 %= nbrTubeSegments.get();
                Indices pointIndices = calculateIndices(t, r, t1, r1);
                setTopLeftTriangleFace(textureIndices, pointIndices);
                setBottomRightTriangleFace(textureIndices, pointIndices);
            }
        }

        private void setTopLeftTriangleFace(Indices textureIndices, Indices pointIndices) {
            faces[facesIndex++] = pointIndices.topLeft;
            faces[facesIndex++] = textureIndices.topLeft;
            faces[facesIndex++] = pointIndices.bottomLeft;
            faces[facesIndex++] = textureIndices.bottomLeft;
            faces[facesIndex++] = pointIndices.topRight;
            faces[facesIndex++] = textureIndices.topRight;
        }

        private void setBottomRightTriangleFace(Indices textureIndices, Indices pointIndices) {
            faces[facesIndex++] = pointIndices.bottomLeft;
            faces[facesIndex++] = textureIndices.bottomLeft;
            faces[facesIndex++] = pointIndices.bottomRight;
            faces[facesIndex++] = textureIndices.bottomRight;
            faces[facesIndex++] = pointIndices.topRight;
            faces[facesIndex++] = textureIndices.topRight;
        }

        private Indices calculateIndices(int t, int r, int t1, int r1) {
            Indices indices = new Indices();
            indices.topLeft = r + t * nbrRingSteps.get();
            indices.bottomLeft = r + t1 * nbrRingSteps.get();
            indices.topRight = r1 + t * nbrRingSteps.get();
            indices.bottomRight = r1 + t1 * nbrRingSteps.get();
            return indices;
        }

        public void setPointsTexCoordsAndFaces(TriangleMesh mesh) {
            mesh.getPoints().setAll(points);
            mesh.getTexCoords().setAll(textureCoords);
            mesh.getFaces().setAll(faces);
        }

        private final class Indices {

            private int topLeft;
            private int topRight;
            private int bottomLeft;
            private int bottomRight;

            private Indices() {
            }

        }
    }

    /**
     * Calculate the texture coordinates based on number of texture repeats.
     *
     * @param nbrRingRepeats number of texture repeats round the ring
     * @param nbrTubeRepeats number of texture repeats round the tube
     */
    private float[] calcTexturePoints(float nbrRingRepeats, float nbrTubeRepeats) {
        float deltaU = nbrRingRepeats / nbrRingSegments.get();
        float deltaV = nbrTubeRepeats / nbrTubeSegments.get();
        var texturePoints = new float[nbrRingSteps.get() * nbrTubeSteps.get() * TWO_PER_TEXTURE_POINT];
        int index = 0;
        for (int t = 0; t < nbrTubeSteps.get(); t++) {
            for (int r = 0; r < nbrRingSteps.get(); r++) {
                texturePoints[index++] = r * deltaU;
                texturePoints[index++] = t * deltaV;
            }
        }
        return texturePoints;
    }

    public DoubleBinding getVolume() {
        return surfaceAreaIntersection().multiply(circumference());
    }

    private DoubleBinding circumference() {
        return ringRad.multiply(TWO_PI);
    }

    private DoubleBinding surfaceAreaIntersection() {
        return tubeRadX.multiply(tubeRadY).multiply(PI);
    }

    public PhongMaterial getPhongMaterial() {
        return material;
    }

    public IntegerProperty nbrRingSegmentsProperty() {
        return nbrRingSegments;
    }

    public IntegerProperty nbrTubeSegmentsProperty() {
        return nbrTubeSegments;
    }

    public DoubleProperty ringRadProperty() {
        return ringRad;
    }

    public DoubleProperty tubeRadXProperty() {
        return tubeRadX;
    }

    public DoubleProperty tubeRadYProperty() {
        return tubeRadY;
    }

    public DoubleProperty tubeZeroPosProperty() {
        return tubeZeroPos;
    }
}
