package hzt.model.custom_shapes;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.fxml.FXMLLoader;
import javafx.scene.shape.MeshView;

import java.io.IOException;

public class Duck extends MeshView {

    private final DoubleProperty scale = new SimpleDoubleProperty();
    public Duck() {
        this(1);
    }

    public Duck(double scale) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(this.getClass().getResource("/model/Duck.fxml"));
            MeshView meshView = fxmlLoader.load();
            setMesh(meshView.getMesh());
            setScale(scale);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void setScale(double scale) {
        this.scale.set(scale);
        setScaleX(scale);
        setScaleY(scale);
        setScaleZ(scale);
    }

    public double getScale() {
        return scale.get();
    }

}
