package hzt.model.custom_shapes;

import javafx.scene.shape.MeshView;
import javafx.scene.shape.TriangleMesh;

public class Pyramid extends MeshView {

    private final TriangleMesh mesh = new TriangleMesh();

    private final double scale;

    public Pyramid(double scale) {
        this.scale = scale;
        buildMesh();
        setMesh(mesh);
        setScaleX(scale);
        setScaleY(scale);
        setScaleZ(scale);
    }

    public Pyramid() {
        this(1);
    }

    private void buildMesh() {
        mesh.getPoints().addAll(
                0.0f, 1.0f, 1.0f,
                1.0f, 1.0f, 0.0f,
                0.0f, 1.0f, -1.0f,
                -1.0f, 1.0f, 0.0f,
                0.0f, -1.0f, 0.0f);
        mesh.getTexCoords().addAll(0.0f, 0.0f);
        mesh.getFaces().addAll(
                0, 0, 4, 0, 1, 0, 1, 0, 4,
                0, 2, 0, 2, 0, 4, 0, 3, 0,
                3, 0, 4, 0, 0, 0, 0, 0, 1,
                0, 2, 0, 0, 0, 2, 0, 3, 0);
        mesh.getFaceSmoothingGroups().addAll(1, 2, 4, 8, 16, 16);
    }

    public double getScale() {
        return scale;
    }
}
