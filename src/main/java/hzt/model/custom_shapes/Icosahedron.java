package hzt.model.custom_shapes;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.shape.MeshView;
import javafx.scene.shape.TriangleMesh;
import javafx.scene.shape.VertexFormat;

import java.util.ArrayList;
import java.util.List;

import static hzt.model.utils.FxUtils.newValue;

public class Icosahedron extends MeshView {

    private static final int DEFAULT_RADIUS = 1;
    private final DoubleProperty radius = new SimpleDoubleProperty();

    public Icosahedron() {
        this(DEFAULT_RADIUS);
    }

    public Icosahedron(double radius) {
        this.radius.set(radius);
        createIcosahedronMesh((radius));
        this.radius.addListener(newValue(this::createIcosahedronMesh));
    }

    private void createIcosahedronMesh(Number scale) {
        // create 12 vertices of a icosahedron
        // 3 point indices, 3 normal indices and 3 texCoord indices per triangle
        final int NUMBER_OF_VERTICES = 12;
        final int POINTS_PER_VERTEX = 3; // x, y, z
        final int NORMALS_PER_VERTEX = 3; // nx, ny, nz
        final int TEX_COORDS_PER_VERTEX = 2; // u, v

        float[] points = new float[NUMBER_OF_VERTICES * POINTS_PER_VERTEX];
        float[] normals = new float[NUMBER_OF_VERTICES * NORMALS_PER_VERTEX];
        float[] texCoords = new float[NUMBER_OF_VERTICES * TEX_COORDS_PER_VERTEX];

        fillThePointsAndNormalsArrays(scale, points, normals);
        // create 20 triangles of the icosahedron
        int[] faces = buildFaces();

        TriangleMesh triangleMesh = new TriangleMesh(VertexFormat.POINT_NORMAL_TEXCOORD);
        triangleMesh.getPoints().setAll(points);
        triangleMesh.getNormals().setAll(normals);
        triangleMesh.getTexCoords().setAll(texCoords);
        triangleMesh.getFaces().setAll(faces);
        setMesh(triangleMesh);
    }

    private static void fillThePointsAndNormalsArrays(Number scale, float[] points, float[] normals) {
        List<Vec3f> dVectorList = getVector3DList();
        int index = 0;
        for (Vec3f vector : dVectorList) {
            Vec3f normalizeVector = vector.normalize();
            points[index] = scale.floatValue() * vector.x;
            normals[index++] = normalizeVector.x;
            points[index] = scale.floatValue() * vector.y;
            normals[index++] = normalizeVector.y;
            points[index] = scale.floatValue() * vector.z;
            normals[index++] = normalizeVector.z;
        }
    }

    private static List<Vec3f> getVector3DList() {
        List<Vec3f> pointsList = new ArrayList<>();
        float goldenRatio = (float) getGoldenRatio();
        pointsList.add(new Vec3f(-1, goldenRatio, 0));
        pointsList.add(new Vec3f(1, goldenRatio, 0));
        pointsList.add(new Vec3f(-1, -goldenRatio, 0));
        pointsList.add(new Vec3f(1, -goldenRatio, 0));

        pointsList.add(new Vec3f(0, -1, goldenRatio));
        pointsList.add(new Vec3f(0, 1, goldenRatio));
        pointsList.add(new Vec3f(0, -1, -goldenRatio));
        pointsList.add(new Vec3f(0, 1, -goldenRatio));

        pointsList.add(new Vec3f(goldenRatio, 0, -1));
        pointsList.add(new Vec3f(goldenRatio, 0, 1));
        pointsList.add(new Vec3f(-goldenRatio, 0, -1));
        pointsList.add(new Vec3f(-goldenRatio, 0, 1));
        return pointsList;
    }

    private static double getGoldenRatio() {
        return (1 + Math.sqrt(5)) / 2;
    }


    private static int[] buildFaces() {
        return new int[]{
                0, 0, 0, 11, 11, 0, 5, 5, 0,
                0, 0, 0, 5, 5, 0, 1, 1, 0,
                0, 0, 0, 1, 1, 0, 7, 7, 0,
                0, 0, 0, 7, 7, 0, 10, 10, 0,
                0, 0, 0, 10, 10, 0, 11, 11, 0,
                1, 1, 0, 5, 5, 0, 9, 9, 0,
                5, 5, 0, 11, 11, 0, 4, 4, 0,
                11, 11, 0, 10, 10, 0, 2, 2, 0,
                10, 10, 0, 7, 7, 0, 6, 6, 0,
                7, 7, 0, 1, 1, 0, 8, 8, 0,
                3, 3, 0, 9, 9, 0, 4, 4, 0,
                3, 3, 0, 4, 4, 0, 2, 2, 0,
                3, 3, 0, 2, 2, 0, 6, 6, 0,
                3, 3, 0, 6, 6, 0, 8, 8, 0,
                3, 3, 0, 8, 8, 0, 9, 9, 0,
                4, 4, 0, 9, 9, 0, 5, 5, 0,
                2, 2, 0, 4, 4, 0, 11, 11, 0,
                6, 6, 0, 2, 2, 0, 10, 10, 0,
                8, 8, 0, 6, 6, 0, 7, 7, 0,
                9, 9, 0, 8, 8, 0, 1, 1, 0
        };
    }

    public double getRadius() {
        return radius.get();
    }

    public void setRadius(double radius) {
        this.radius.set(radius);
    }

    private static class Vec3f {

        private final float x;
        private final float y;
        private final float z;

        public Vec3f(float x, float y, float z) {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public Vec3f normalize() {
            Double magnitude = magnitude();
            return magnitude.intValue() == 0 ? new Vec3f(0F, 0F, 0F) :
                    new Vec3f(x / magnitude.floatValue(), y / magnitude.floatValue(), z / magnitude.floatValue());
        }

        public Double magnitude() {
            return Math.sqrt(x * x + y * y + z * z);
        }
    }

}
