package hzt.model.custom_shapes;

import javafx.animation.Interpolator;
import javafx.animation.RotateTransition;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.css.Styleable;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.transform.Rotate;
import javafx.util.Duration;

import java.io.IOException;

public class Scooter extends Group {

    private final DoubleProperty scale = new SimpleDoubleProperty();
    public Scooter() {
        this(1);
    }

    public Scooter(double scale) {
        try {
            var fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/model/Scooter.fxml"));
            Group scooter = fxmlLoader.load();
            super.getChildren().add(scooter);
            animateWheels(scooter);
            setScale(scale);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    private void setScale(double scale) {
        this.scale.set(scale);
        setScaleX(scale);
        setScaleY(scale);
        setScaleZ(scale);
    }

    public double getScale() {
        return scale.get();
    }

    private void animateWheels(Group scooter) {
        scooter.getChildren()
                .stream()
                .filter(this::isRim)
                .forEach(this::animateRim);
    }

    public boolean isRim(Styleable view) {
        return "RimFront".equals(view.getId()) || "RimRear".equals(view.getId());
    }

    public void animateRim(Node node) {
        final var SECONDS = 0.33;
        var rt = new RotateTransition(Duration.seconds(SECONDS), node);
        rt.setCycleCount(Integer.MAX_VALUE);
        rt.setAxis(Rotate.X_AXIS);
        final var FULL_CIRCLE = 360;
        rt.setByAngle(FULL_CIRCLE);
        rt.setInterpolator(Interpolator.LINEAR);
        rt.play();
    }

}
