package hzt.model;

import javafx.geometry.Dimension2D;
import javafx.scene.paint.Color;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static javafx.scene.paint.Color.*;

public final class AppConstants {

    private static final Logger LOGGER = LoggerFactory.getLogger(AppConstants.class);

    public static final int ACCELERATION_CORRECTION_FACTOR = 3;

    public static final int INIT_FRAME_RATE = 30; //f/s
    public static final String ANSI_RESET = "\u001B[0m";

    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String TITLE = "Flocking simulation 3D";
    public static final String DOTTED_LINE = "----------------------------------------------------------------------\n";
    public static final String CLOSING_MESSAGE = ANSI_BLUE + "See you next Time! :)" + ANSI_RESET;
    // camera properties

    public static final double INIT_CAMERA_TRANSLATE_Z =  -800;
    public static final double INIT_FIELD_OF_VIEW_CAMERA = 30;

    public static final Color INIT_BG_COLOR = BLACK;

    public static final Color INIT_OBJECT_UNIFORM_COLOR = ORANGE;
    public static final Color INIT_SELECTED_BOID_COLOR = RED;
    public static final Color INIT_AQUARIUM_COLOR = DARKBLUE;
    public static final Dimension2D MIN_STAGE_DIMENSION;

    public static final Dimension2D INIT_SCENE_DIMENSION;
    private static final Properties PROPS = configProperties();

    public static final int DEFAULT_SCENE_WIDTH = 1200;

    public static final int DEFAULT_SCENE_HEIGHT = 800;

    private AppConstants() {
    }

    static {
        int sceneWidthProp = parsedIntAppProp("init_scene_width", DEFAULT_SCENE_WIDTH);
        int sceneHeightProp = parsedIntAppProp("init_scene_height", DEFAULT_SCENE_HEIGHT);
        INIT_SCENE_DIMENSION = new Dimension2D(sceneWidthProp, sceneHeightProp);
        MIN_STAGE_DIMENSION = determineMinStageDimension();
    }

    public static String getProperty(String property, String defaultVal) {
        return PROPS.getProperty(property, defaultVal);
    }

    public static double parsedDoubleAppProp(String property, double defaultVal) {
        double value = defaultVal;
        String propertyVal = PROPS.getProperty(property);
        if (propertyVal != null) {
            try {
                value = Double.parseDouble(propertyVal);
            } catch (NumberFormatException e) {
                LOGGER.warn(String.format("Property '%s' with value '%s' could not be parsed to a double... " +
                        "Falling back to default: %f...", property, propertyVal, defaultVal));
            }
        } else {
            LOGGER.warn("Property '{}' not found. Falling back to default: %{}", property, defaultVal);
        }
        return value;
    }

    public static int parsedIntAppProp(String property, int defaultVal) {
        int value = defaultVal;
        String propertyVal = PROPS.getProperty(property);
        if (propertyVal != null) {
            try {
                value = Integer.parseInt(propertyVal);
            } catch (NumberFormatException e) {
                LOGGER.warn(String.format("Property '%s' with value '%s' could not be parsed to an int... " +
                        "Falling back to default: %d...", property, propertyVal, defaultVal));
            }
        } else {
            LOGGER.warn("Property '{}' not found. Falling back to default:{}", property, defaultVal);
        }
        return value;
    }


    private static Dimension2D determineMinStageDimension() {
        final var DEFAULT_MIN_STAGE_WIDTH = 750;
        final var DEFAULT_MIN_STAGE_HEIGHT = 500;
        double minStageWidth = INIT_SCENE_DIMENSION.getWidth() < DEFAULT_MIN_STAGE_WIDTH ?
                INIT_SCENE_DIMENSION.getWidth() : DEFAULT_MIN_STAGE_WIDTH;
        double minStageHeight = INIT_SCENE_DIMENSION.getHeight() < DEFAULT_MIN_STAGE_HEIGHT ?
                INIT_SCENE_DIMENSION.getHeight() : DEFAULT_MIN_STAGE_HEIGHT;
        return new Dimension2D(minStageWidth, minStageHeight);
    }

    private static Properties configProperties() {
        var properties = new Properties();
        var name = "/app.properties";
        try (InputStream stream = AppConstants.class.getResourceAsStream(name)) {
            if (stream != null) {
                properties.load(stream);
            } else {
                LOGGER.warn("{} not found...", name);
            }
        } catch (IOException e) {
            LOGGER.warn("{} not found...", name ,e);
        }
        return properties;
    }

    public enum Scene {

        MAIN_SCENE("mainScene.fxml", "Main Scene"),
        ABOUT_SCENE("aboutScene.fxml", "About Scene");

        private final String fxmlFileName;
        private final String englishDescription;

        Scene(String fxmlFileName, String englishDescription) {
            this.fxmlFileName = fxmlFileName;
            this.englishDescription = englishDescription;
        }

        public String getFxmlFileName() {
            return fxmlFileName;
        }

        public String getEnglishDescription() {
            return englishDescription;
        }
    }

}
