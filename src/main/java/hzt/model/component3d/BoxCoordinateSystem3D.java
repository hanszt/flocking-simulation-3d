package hzt.model.component3d;

import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Box;

public class BoxCoordinateSystem3D extends CoordinateSystem3D {

    public static final double AXIS_LENGTH = 500.0;

    public BoxCoordinateSystem3D() {
        var redMaterial = new PhongMaterial();
        redMaterial.setDiffuseColor(Color.DARKRED);
        redMaterial.setSpecularColor(Color.RED);
        var greenMaterial = new PhongMaterial();
        greenMaterial.setDiffuseColor(Color.DARKGREEN);
        greenMaterial.setSpecularColor(Color.GREEN);
        var blueMaterial = new PhongMaterial();
        blueMaterial.setDiffuseColor(Color.DARKBLUE);
        blueMaterial.setSpecularColor(Color.BLUE);
        var xAxis = new Box(AXIS_LENGTH, 1, 1);
        xAxis.setMaterial(redMaterial);
        var yAxis = new Box(1, AXIS_LENGTH, 1);
        yAxis.setMaterial(greenMaterial);
        var zAxis = new Box(1, 1, AXIS_LENGTH);
        zAxis.setMaterial(blueMaterial);
        super.getChildren().addAll(xAxis, yAxis, zAxis);
    }
}
