package hzt.model.component3d;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ObservableList;
import javafx.geometry.Point3D;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.paint.Material;
import javafx.scene.shape.Line;

public class Path3D extends Group {

    private final DoubleProperty lineWidth = new SimpleDoubleProperty();
    private final ObjectProperty<Material> material = new SimpleObjectProperty<>();

    public Path3D() {
        super();
    }

    public void addLine(Point3D currentPosition, Point3D prevPosition) {
        if (!currentPosition.equals(Point3D.ZERO) && !prevPosition.equals(Point3D.ZERO)) {
            Line3D line = new Line3D();
            line.setStart(prevPosition);
            line.setEnd(currentPosition);
            line.materialProperty().bind(material);
            line.widthProperty().bind(lineWidth);
            getChildren().add(line);
        }
    }

    public void setLineWidth(double lineWidth) {
        this.lineWidth.set(lineWidth);
    }

   public void fadeOut() {
        int size = getChildren().size();
        ObservableList<Node> children = getChildren();
        for (int i = 0, childrenSize = children.size(); i < childrenSize; i++) {
            Node node = children.get(i);
            Line line = (Line) node;
            line.setOpacity((float) i / size);
        }
    }

    public void removeLine(int index) {
        getChildren().remove(index);
    }

    public ObservableList<Node> getElements() {
        return getChildren();
    }

    public void setMaterial(Material material) {
        this.material.set(material);
    }

}
