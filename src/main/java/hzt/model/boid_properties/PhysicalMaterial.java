package hzt.model.boid_properties;

import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;

public class PhysicalMaterial extends PhongMaterial {

    private static final int SPECULAR_POWER = 64;
    private final double density;// kg/m^3

    public PhysicalMaterial(Color color, double density) {
        super(color);
        this.density = density;
        setSpecularColor(Color.WHITE);
        setSpecularPower(SPECULAR_POWER);
    }

    public double getDensity() {
        return density;
    }
}
