package hzt.model.boid_properties;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.paint.Color;

public class FlockProperties {

    private final DoubleProperty maxBallSize = new SimpleDoubleProperty();

    private final DoubleProperty perceptionRadiusRatio = new SimpleDoubleProperty();
    private final DoubleProperty repelRadiusRatio = new SimpleDoubleProperty();

    private final BooleanProperty velocityVectorsVisible = new SimpleBooleanProperty();
    private final BooleanProperty accelerationVectorsVisible = new SimpleBooleanProperty();
    private final BooleanProperty perceptionSpheresVisible = new SimpleBooleanProperty();
    private final BooleanProperty  repelSphereVisible = new SimpleBooleanProperty();
    private final BooleanProperty showConnections = new SimpleBooleanProperty();

    private final BooleanProperty perceptionVisibleSelected = new SimpleBooleanProperty();

    private final ObjectProperty<Color> boidColor = new SimpleObjectProperty<>();

    public double getMaxBallSize() {
        return maxBallSize.get();
    }

    public DoubleProperty maxBallSizeProperty() {
        return maxBallSize;
    }

    public double getPerceptionRadiusRatio() {
        return perceptionRadiusRatio.get();
    }

    public DoubleProperty perceptionRadiusRatioProperty() {
        return perceptionRadiusRatio;
    }

    public double getRepelRadiusRatio() {
        return repelRadiusRatio.get();
    }

    public DoubleProperty repelRadiusRatioProperty() {
        return repelRadiusRatio;
    }

    public boolean isVelocityVectorsVisible() {
        return velocityVectorsVisible.get();
    }

    public BooleanProperty velocityVectorsVisibleProperty() {
        return velocityVectorsVisible;
    }

    public boolean isAccelerationVectorsVisible() {
        return accelerationVectorsVisible.get();
    }

    public BooleanProperty accelerationVectorsVisibleProperty() {
        return accelerationVectorsVisible;
    }

    public boolean isPerceptionSpheresVisible() {
        return perceptionSpheresVisible.get();
    }

    public BooleanProperty perceptionSpheresVisibleProperty() {
        return perceptionSpheresVisible;
    }

    public boolean isRepelSphereVisible() {
        return repelSphereVisible.get();
    }

    public BooleanProperty repelSphereVisibleProperty() {
        return repelSphereVisible;
    }

    public boolean isPerceptionVisibleSelected() {
        return perceptionVisibleSelected.get();
    }

    public BooleanProperty perceptionVisibleSelectedProperty() {
        return perceptionVisibleSelected;
    }

    public Color getBoidColor() {
        return boidColor.get();
    }

    public ObjectProperty<Color> boidColorProperty() {
        return boidColor;
    }

    public BooleanProperty showConnectionsProperty() {
        return showConnections;
    }
}
