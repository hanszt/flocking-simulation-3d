package hzt.model.boid3d;

import hzt.model.boid_properties.PhysicalMaterial;
import hzt.model.custom_shapes.Duck;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Shape3D;

import java.util.Optional;

public class Duck3D extends Boid3D {

    private static int nextDuck = 0;

    public Duck3D(double size, Color color) {
        this(new Duck(size), "Duck" + ++nextDuck, new PhysicalMaterial(color, DENSITY));
    }

    Duck3D(Shape3D body, String name, PhysicalMaterial bodyMaterial) {
        super(body, name, bodyMaterial);
        var material = (PhongMaterial) body.getMaterial();
        Optional.ofNullable(getClass().getResourceAsStream("/model/duckCM.png"))
                .ifPresent(inputStream -> material.setDiffuseMap(new Image(inputStream)));
    }

    @Override
    public double getMass() {
        return getMassByDensityRadiusAndHeight();
    }

    //TODO: refine
    private double getMassByDensityRadiusAndHeight() {
        double volume = ((Duck) getBody()).getScale();
        return getBodyMaterial().getDensity() * volume;
    }

    //TODO: refine
    @Override
    public double getDistanceFromCenterToOuterEdge() {
        return ((Duck) getBody()).getScale();
    }
}
