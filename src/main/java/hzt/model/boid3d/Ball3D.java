package hzt.model.boid3d;

import hzt.model.boid_properties.PhysicalMaterial;
import javafx.scene.paint.Color;
import javafx.scene.shape.Box;
import javafx.scene.shape.Sphere;

public class Ball3D extends Boid3D {

    private static final double DEFAULT_RADIUS;

    static {
        final var box = new Box();
        final var boxVolume = box.getWidth() * box.getHeight() * box.getDepth();
        DEFAULT_RADIUS = Math.pow(3 * boxVolume / (4 * Math.PI), 1 / 3.);
    }

    private static int nextBall = 0;

    public Ball3D(Color color) {
        this(DEFAULT_RADIUS, color);
    }

    public Ball3D(double radius, Color color) {
        this(radius, "Ball" + ++nextBall, new PhysicalMaterial(color, DENSITY));
    }

    public Ball3D(double radius, String name, PhysicalMaterial material) {
        super(new Sphere(radius), name, material);
    }

    @Override
    public double getMass() {
        return getMassByDensityAndRadius();
    }

    @Override
    public double getDistanceFromCenterToOuterEdge() {
        return ((Sphere) getBody()).getRadius();
    }

    private double getMassByDensityAndRadius() {
        double volume = 4 * Math.PI * Math.pow(((Sphere) getBody()).getRadius(), 3) / 3;
        return getBodyMaterial().getDensity() * volume;
    }

}
