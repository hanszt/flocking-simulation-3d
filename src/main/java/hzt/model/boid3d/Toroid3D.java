package hzt.model.boid3d;

import hzt.model.boid_properties.PhysicalMaterial;
import hzt.model.custom_shapes.Toroid;
import javafx.scene.Node;
import javafx.scene.paint.Color;

public class Toroid3D extends Boid3D {

    private static int nextToroid = 0;

    public Toroid3D(double maxSize, Color color) {
        this(new Toroid(maxSize), color);
    }
    public Toroid3D(Node body, Color color) {
        super(body, "Toroid" + ++nextToroid, new PhysicalMaterial(color, DENSITY));
    }

    @Override
    public double getMass() {
        var toroid = (Toroid) getBody();
        return getBodyMaterial().getDensity() * toroid.getVolume().get();
    }

    @Override
    public double getDistanceFromCenterToOuterEdge() {
        var toroid = (Toroid) getBody();
        return toroid.ringRadProperty().get() + toroid.tubeRadXProperty().get();
    }
}
