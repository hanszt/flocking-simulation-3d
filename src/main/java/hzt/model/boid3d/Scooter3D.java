package hzt.model.boid3d;

import hzt.model.boid_properties.PhysicalMaterial;
import hzt.model.component3d.Line3D;
import hzt.model.custom_shapes.Scooter;
import hzt.model.utils.FxUtils;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.shape.Shape3D;

public class Scooter3D extends Boid3D {

    private static int nextScooter = 0;

    public Scooter3D(double size, Color color) {
        this(new Scooter(size), "Scooter" + ++nextScooter, new PhysicalMaterial(color, DENSITY));
    }

    Scooter3D(Group body, String name, PhysicalMaterial bodyMaterial) {
        super(body, name, bodyMaterial);
    }

    @Override
    public double getMass() {
        return getMassByDensityRadiusAndHeight();
    }

    //TODO: refine
    private double getMassByDensityRadiusAndHeight() {
        Scooter scooter = (Scooter) getBody();
        double volume = scooter.getScale();
        return getBodyMaterial().getDensity() * volume;
    }

    //TODO: refine
    @Override
    public double getDistanceFromCenterToOuterEdge() {
        return ((Scooter) getBody()).getScale();
    }

    @Override
    protected void setMaterialForChildren(Group body) {
        FxUtils.allDescendents(body).stream()
                .filter(Scooter3D::isBodyComponent).map(Shape3D.class::cast)
                .forEach(s -> s.setMaterial(getBodyMaterial()));
    }

    @Override
    protected void bindMaterialPropertyToShape3DChildIfPresent(Line3D line) {
        FxUtils.allDescendents((Group) getBody()).stream()
                .filter(Scooter3D::isBodyComponent)
                .map(Shape3D.class::cast)
                .findFirst()
                .ifPresent(shape3D -> line.materialProperty().bind(shape3D.materialProperty()));
    }

    private static boolean isBodyComponent(Node n) {
        String id = n.getId();
        var isFrontBodyComponent = "ColumnFront".equals(id) ||
                "FenderFront_backface".equals(id) ||
                "FenderFront_frontface".equals(id);
        var isSmallBodyComponent = "MirrorsColor".equals(id) ||
                "HoodRear".equals(id) ||
                "PassengerGuardFront".equals(id);
        var condition = "FenderRear_backface".equals(id) ||
                "FenderRear_frontface".equals(id) ||
                "InstrumentMount".equals(id);
        boolean isBodyComponent = isFrontBodyComponent || condition || isSmallBodyComponent;
        return n instanceof Shape3D && isBodyComponent;
    }

}
