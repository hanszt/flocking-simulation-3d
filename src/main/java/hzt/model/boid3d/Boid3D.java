package hzt.model.boid3d;

import hzt.model.Flock;
import hzt.model.boid_properties.FlockProperties;
import hzt.model.boid_properties.PhysicalMaterial;
import hzt.model.component3d.Line3D;
import hzt.model.component3d.Path3D;
import hzt.model.component3d.VisibleVector3D;
import hzt.model.controls3d.Translation3DKeyFilter;
import hzt.model.utils.FxUtils;
import javafx.collections.ObservableList;
import javafx.geometry.Point3D;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.DrawMode;
import javafx.scene.shape.Shape3D;
import javafx.scene.shape.Sphere;
import javafx.util.Duration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static hzt.model.AppConstants.ACCELERATION_CORRECTION_FACTOR;
import static hzt.model.utils.MovementManager.limit;
import static hzt.model.utils.RandomGenerator.getRandomAxisOfRotation;
import static hzt.model.utils.RandomGenerator.getRandomPoint3D;

public abstract class Boid3D extends Group {

    private static final int MAX_VECTOR_LENGTH = 120;
    private static final Point3D INIT_MAX_SPEED = new Point3D(5, 5, 5);
    private static final double BOUNDARY_OPACITY = 0.1;
    private static final double INIT_BOID_ROTATION_ANGLE = 45D;
    protected static final double DENSITY = 100; // kg/m^3
    private static final int LINE_STROKE_WIDTH = 2;

    private final String name;
    private final Node body;
    private final Color initColor;

    private final Sphere perceptionSphere = new Sphere();
    private final Sphere repelSphere = new Sphere();
    private final Line3D visibleAccelerationVector = new VisibleVector3D();
    private final Line3D visibleVelocityVector = new VisibleVector3D();
    private final Path3D path = new Path3D();
    private final Group connections = new Group();
    private final Map<Boid3D, Line3D> perceptionRadiusMap = new HashMap<>();
    private final PhysicalMaterial bodyMaterial;
    private final PhysicalMaterial boundaryMaterial;

    private final Translation3DKeyFilter translation3DKeyFilter = new Translation3DKeyFilter();

    private Point3D velocity; // m/s
    private Point3D acceleration; // m/s^2

    Boid3D(Node body, String name, PhysicalMaterial bodyMaterial) {
        this.body = body;
        this.initColor = bodyMaterial.getDiffuseColor();
        this.bodyMaterial = bodyMaterial;
        var color = new Color(initColor.getRed(), initColor.getGreen(), initColor.getBlue(), BOUNDARY_OPACITY);
        this.boundaryMaterial = new PhysicalMaterial(color, DENSITY);
        this.name = name;

        this.velocity = getRandomPoint3D(INIT_MAX_SPEED.multiply(-1), INIT_MAX_SPEED);
        this.acceleration = Point3D.ZERO;
        super.getChildren().addAll(body, perceptionSphere, repelSphere);
        configureComponents();
    }

    public void addBoidComponents(ObservableList<Node> children) {
        children.addAll(visibleVelocityVector, visibleAccelerationVector, connections);
    }

    public void removeBoidComponents(ObservableList<Node> children) {
        children.removeAll(visibleVelocityVector, visibleAccelerationVector, connections);
    }

    public void update(Duration deltaT, double frictionFactor, double maxVelocity, double maxAcceleration) {
        Flock flock = (Flock) getParent();
        FlockProperties flockProperties = flock.getFlockProperties();
        double correctedMaxAcceleration = maxAcceleration / (deltaT.toSeconds() * ACCELERATION_CORRECTION_FACTOR);
        translation3DKeyFilter.setUserInputSize(correctedMaxAcceleration);
        updateAcceleration(frictionFactor);
        updatePosition(deltaT, maxVelocity, correctedMaxAcceleration);
        lazyUpdateVisibleVectors(flockProperties);
        updateBoidsInPerceptionRadiusMap();
    }

    private void updateAcceleration(double frictionFactor) {
        acceleration = Point3D.ZERO;
        Flock flock = (Flock) this.getParent();
        Set<Boid3D> ballsSet = perceptionRadiusMap.keySet();
        Point3D physicsEngineAcceleration = flock.getFlockingSim().getTotalAcceleration(this, ballsSet);
        acceleration = acceleration.add(physicsEngineAcceleration);
        acceleration = acceleration.add(friction(frictionFactor));
        acceleration = acceleration.add(translation3DKeyFilter.getUserInputAcceleration());
    }

    public void updatePosition(Duration frameDuration, double maxVelocity, double maxAcceleration) {
        Point3D position = getBodyTranslate();
        double deltaTSeconds = frameDuration.toSeconds();
        acceleration = limit(maxAcceleration, acceleration);
        velocity = velocity.add(acceleration.multiply(deltaTSeconds));
        velocity = limit(maxVelocity, velocity);
        position = position.add(velocity.multiply(deltaTSeconds));
        setBodyTranslate(position);
    }

    public Point3D getBodyTranslate() {
        return new Point3D(body.getTranslateX(), body.getTranslateY(), body.getTranslateZ());
    }

    public void setBodyTranslate(Point3D point3D) {
        body.setTranslateX(point3D.getX());
        body.setTranslateY(point3D.getY());
        body.setTranslateZ(point3D.getZ());
    }

    private void lazyUpdateVisibleVectors(FlockProperties flockProperties) {
        if (flockProperties.isVelocityVectorsVisible()) {
            final var CORRECTION_VEL = 800;
            updateVisibleVector(visibleVelocityVector, velocity, CORRECTION_VEL);
        }
        if (flockProperties.isAccelerationVectorsVisible()) {
            final var CORRECTION = 1000;
            updateVisibleVector(visibleAccelerationVector, acceleration, CORRECTION);
        }
    }

    private static void updateVisibleVector(Line3D line, Point3D vector, double correction) {
        Point3D begin = Point3D.ZERO;
        Point3D end = begin.add(vector);
        Point3D unitVector = end.subtract(begin).normalize();
        end = begin.add(unitVector.multiply(MAX_VECTOR_LENGTH * vector.magnitude() / correction));
        double visibleVectorMagnitude = end.subtract(begin).magnitude();
        if (visibleVectorMagnitude > MAX_VECTOR_LENGTH) {
            end = begin.add(unitVector.multiply(MAX_VECTOR_LENGTH));
        }
        line.setEnd(end);
    }

    public Point3D friction(double frictionFactor) {
        Point3D decelerationDir = velocity.multiply(-1);
        return decelerationDir.multiply(frictionFactor);
    }

    private void updateBoidsInPerceptionRadiusMap() {
        Flock flock = (Flock) getParent();
        flock.getChildrenUnmodifiable().stream()
                .filter(Boid3D.class::isInstance)
                .map(Boid3D.class::cast)
                .filter(node -> !node.equals(this))
                .forEach(this::determineIfBoidIsInPerceptionSphere);
    }

    private void determineIfBoidIsInPerceptionSphere(Boid3D other) {
        Point3D positionThis = getBodyTranslate();
        Point3D positionOther = other.getBodyTranslate();
        double distance = positionOther.subtract(positionThis).magnitude();
        if (distance >= perceptionSphere.getRadius()) {
            Line3D connectionToOther = perceptionRadiusMap.remove(other);
            connections.getChildren().remove(connectionToOther);
        } else {
            perceptionRadiusMap.computeIfAbsent(other, this::newConnection);
            Line3D connection = perceptionRadiusMap.get(other);
            if (!connections.getChildren().contains(connection)) {
                connections.getChildren().add(connection);
            }
        }
    }

    private Line3D newConnection(Boid3D other) {
        Line3D lineToOther = new Line3D();
        lineToOther.widthProperty().set(LINE_STROKE_WIDTH);
        lineToOther.materialProperty().bind(repelSphere.materialProperty());
        lineToOther.defineStartAndEndpoint(this.body, other.body);
        // ignores user input
        lineToOther.setDisable(true);
        return lineToOther;
    }

    private void configureComponents() {
        if (body instanceof Shape3D) {
            ((Shape3D) body).setMaterial(bodyMaterial);
        } else if (body instanceof Group) {
            setMaterialForChildren((Group) body);
        } else {
            throw new IllegalStateException();
        }
        setInitialRotation();
        configureBoundarySphere(perceptionSphere);
        perceptionSphere.setDisable(true);
        configureBoundarySphere(repelSphere);
        configureLine3D(visibleVelocityVector);
        configureLine3D(visibleAccelerationVector);
        path.setManaged(false);
    }

    protected void setMaterialForChildren(Group body) {
        FxUtils.allDescendents(body).stream()
                .filter(Shape3D.class::isInstance).map(Shape3D.class::cast)
                .forEach(s -> s.setMaterial(bodyMaterial));
    }

    private void configureBoundarySphere(Sphere sphere) {
        sphere.setDrawMode(DrawMode.LINE);
        sphere.setMaterial(boundaryMaterial);
        sphere.translateXProperty().bind(body.translateXProperty());
        sphere.translateYProperty().bind(body.translateYProperty());
        sphere.translateZProperty().bind(body.translateZProperty());
    }


    private void configureLine3D(Line3D line) {
        if (body instanceof Shape3D) {
            line.materialProperty().bind(((Shape3D) body).materialProperty());
        } else if (body instanceof Parent) {
            bindMaterialPropertyToShape3DChildIfPresent(line);
        } else {
            throw new IllegalStateException();
        }
        line.widthProperty().set(LINE_STROKE_WIDTH);
        line.startXProperty().bind(body.translateXProperty());
        line.startYProperty().bind(body.translateYProperty());
        line.startZProperty().bind(body.translateZProperty());
    }

    protected void bindMaterialPropertyToShape3DChildIfPresent(Line3D line) {
        FxUtils.allDescendents((Parent) body).stream()
                .filter(Shape3D.class::isInstance)
                .map(Shape3D.class::cast)
                .findFirst()
                .ifPresent(shape3D -> line.materialProperty().bind(shape3D.materialProperty()));
    }

    private void setInitialRotation() {
        setRotate(INIT_BOID_ROTATION_ANGLE);
        setRotationAxis(getRandomAxisOfRotation());
    }

    public abstract double getMass();

    public abstract double getDistanceFromCenterToOuterEdge();

    public void addMouseFunctionality() {
        body.setOnMousePressed(this::mousePressedAction);
        repelSphere.setOnMousePressed(this::mousePressedAction);
    }

    public void addKeyControlForAcceleration() {
        Scene scene = ((Flock) getParent()).getMainScene();
        scene.addEventFilter(KeyEvent.KEY_PRESSED, translation3DKeyFilter.getKeyPressed());
        scene.addEventFilter(KeyEvent.KEY_RELEASED, translation3DKeyFilter.getKeyReleased());
    }

    public void removeKeyControlsForAcceleration() {
        Scene scene = ((Flock) getParent()).getMainScene();
        translation3DKeyFilter.resetKeyPressed();
        scene.removeEventFilter(KeyEvent.KEY_PRESSED, translation3DKeyFilter.getKeyPressed());
        scene.removeEventFilter(KeyEvent.KEY_RELEASED, translation3DKeyFilter.getKeyReleased());
    }

    private void mousePressedAction(MouseEvent e) {
        Flock flock = (Flock) getParent();
        FlockProperties properties = flock.getFlockProperties();
        Boid3D prevSelected = flock.getSelectedBoid();
        updateColor(flock.getSelectedBoidColor());
        perceptionSphere.setVisible(properties.isPerceptionVisibleSelected());
        velocity = Point3D.ZERO;
        if (!this.equals(prevSelected)) {
            addKeyControlForAcceleration();
            if (prevSelected != null) {
                prevSelected.removeKeyControlsForAcceleration();
                prevSelected.getPerceptionSphere().setVisible(flock.getFlockProperties().isPerceptionSpheresVisible());
                prevSelected.updateColor(boidColor(flock, prevSelected));
            }
            flock.setSelectedBoid(this);
        }
    }

    private static Color boidColor(Flock flock, Boid3D selected) {
        return flock.getFlockType().isRandom() ? selected.initColor : flock.getFlockProperties().getBoidColor();
    }

    public void updateColor(Color color) {
        Color boundaryColor = new Color(color.getRed(), color.getGreen(), color.getBlue(), BOUNDARY_OPACITY);
        bodyMaterial.setDiffuseColor(color);
        boundaryMaterial.setDiffuseColor(boundaryColor.darker().darker());
    }

    public void setPerceptionRadiusByRatio(double radiusRatio) {
        this.perceptionSphere.setRadius(radiusRatio * getDistanceFromCenterToOuterEdge());
    }

    public float getRepelRadius() {
        return (float) repelSphere.getRadius();
    }

    public void setRepelRadiusByRatio(double repelRadiusRatio) {
        this.repelSphere.setRadius(repelRadiusRatio * getDistanceFromCenterToOuterEdge());
    }

    public void setBoidParams(FlockProperties flockProperties) {
        visibleVelocityVector.setVisible(flockProperties.isVelocityVectorsVisible());
        visibleAccelerationVector.setVisible(flockProperties.isAccelerationVectorsVisible());

        setRepelRadiusByRatio(flockProperties.getRepelRadiusRatio());
        repelSphere.setVisible(flockProperties.isRepelSphereVisible());

        setPerceptionRadiusByRatio(flockProperties.getPerceptionRadiusRatio());
        perceptionSphere.setVisible(flockProperties.isPerceptionSpheresVisible());
        connections.visibleProperty().bind(flockProperties.showConnectionsProperty());
    }

    public String getName() {
        return name;
    }

    public Node getBody() {
        return body;
    }

    public PhysicalMaterial getBodyMaterial() {
        return bodyMaterial;
    }

    public Map<Boid3D, Line3D> getPerceptionRadiusMap() {
        return perceptionRadiusMap;
    }

    public Sphere getRepelSphere() {
        return repelSphere;
    }

    public Point3D getVelocity() {
        return velocity;
    }

    public Line3D getVisibleAccelerationVector() {
        return visibleAccelerationVector;
    }

    public Line3D getVisibleVelocityVector() {
        return visibleVelocityVector;
    }

    public Point3D getAcceleration() {
        return acceleration;
    }

    public Sphere getPerceptionSphere() {
        return perceptionSphere;
    }

    public List<Line3D> getConnections() {
        return connections.getChildren().stream()
                .filter(Line3D.class::isInstance)
                .map(Line3D.class::cast)
                .collect(Collectors.toList());
    }
}
