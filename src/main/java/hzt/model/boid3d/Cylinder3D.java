package hzt.model.boid3d;

import hzt.model.boid_properties.PhysicalMaterial;
import javafx.scene.paint.Color;
import javafx.scene.shape.Cylinder;

public class Cylinder3D extends Boid3D {

    private static int nextCylinder = 0;

    public Cylinder3D(double size, Color color) {
        this(new Cylinder(size / 2, size), color);
    }

    public Cylinder3D(Cylinder cylinder, Color color) {
        super(cylinder, "Cylinder" + ++nextCylinder, new PhysicalMaterial(color, DENSITY));
    }

    @Override
    public double getMass() {
        return getMassByDensityRadiusAndHeight();
    }

    @Override
    public double getDistanceFromCenterToOuterEdge() {
        Cylinder body = (Cylinder) getBody();
        double radius = body.getRadius();
        double height = body.getHeight();
        double diagonal = Math.sqrt(4 * radius * radius + height * height);
        return diagonal / 2;
    }

    private double getMassByDensityRadiusAndHeight() {
        Cylinder cylinder = (Cylinder) getBody();
        double volume = Math.PI * Math.pow(cylinder.getRadius(), 2) * cylinder.getHeight();
        return getBodyMaterial().getDensity() * volume;
    }
}
