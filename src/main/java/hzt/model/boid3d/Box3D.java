package hzt.model.boid3d;

import hzt.model.boid_properties.PhysicalMaterial;
import javafx.scene.paint.Color;
import javafx.scene.shape.Box;

public class Box3D extends Boid3D {

    private static int nextBox = 0;

    public Box3D(Color color) {
        this(new Box(), "Box" + ++nextBox, color);
    }

    public Box3D(double size, Color color) {
        this(new Box(size, size, size), "Box" + ++nextBox, color);
    }

    public Box3D(Box box, String name, Color color) {
        super(box, name, new PhysicalMaterial(color, DENSITY));
    }

    @Override
    public double getMass() {
        return getMassByDensityWidthHeightAndDepth();
    }

    @Override
    public double getDistanceFromCenterToOuterEdge() {
        var box = (Box) getBody();
        double diagonal = Math.sqrt(
                box.getWidth() * box.getWidth() +
                box.getHeight() + box.getHeight() +
                box.getDepth() * box.getDepth());
        final var TWO = 2;
        return diagonal / TWO;
    }

    private double getMassByDensityWidthHeightAndDepth() {
        var box = (Box) getBody();
        double volume = box.getWidth() * box.getHeight() * box.getDepth();
        return getBodyMaterial().getDensity() * volume;
    }


}
