package hzt.model.boid3d;

import hzt.model.boid_properties.PhysicalMaterial;
import hzt.model.custom_shapes.Icosahedron;
import javafx.scene.paint.Color;

public class Icosahedron3D extends Boid3D {

    private static int nextIcosahedron = 0;

    public Icosahedron3D(Color color) {
        this(new Icosahedron(), "Icosahedron" + ++nextIcosahedron, color);
    }

    public Icosahedron3D(double size, Color color) {
        this(new Icosahedron(size), "Icosahedron" + ++nextIcosahedron, color);
    }

    public Icosahedron3D(Icosahedron icosahedron, String name, Color color) {
        super(icosahedron, name, new PhysicalMaterial(color, DENSITY));
    }

    @Override
    public double getMass() {
        return getMassByDensityAndRadius();
    }

    @Override
    public double getDistanceFromCenterToOuterEdge() {
        return ((Icosahedron) getBody()).getRadius();
    }

    private double getMassByDensityAndRadius() {
        double volume = 4 * Math.PI * Math.pow(((Icosahedron) getBody()).getRadius(), 3) / 3;
        return getBodyMaterial().getDensity() * volume;
    }
}
