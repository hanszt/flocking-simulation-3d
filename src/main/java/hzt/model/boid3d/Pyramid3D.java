package hzt.model.boid3d;

import hzt.model.boid_properties.PhysicalMaterial;
import hzt.model.custom_shapes.Pyramid;
import javafx.scene.paint.Color;
import javafx.scene.shape.Shape3D;

public class Pyramid3D extends Boid3D {

    private static int nextPyramid = 0;

    public Pyramid3D(double size, Color color) {
        this(new Pyramid(size), "Pyramid" + ++nextPyramid, new PhysicalMaterial(color, DENSITY));
    }

    Pyramid3D(Shape3D body, String name, PhysicalMaterial bodyMaterial) {
        super(body, name, bodyMaterial);
    }

    @Override
    public double getMass() {
        return getMassByDensityRadiusAndHeight();
    }

    //TODO: refine
    private double getMassByDensityRadiusAndHeight() {
        double volume = ((Pyramid) getBody()).getScale();
        return getBodyMaterial().getDensity() * volume;
    }

    //TODO: refine
    @Override
    public double getDistanceFromCenterToOuterEdge() {
        return ((Pyramid) getBody()).getScale();
    }
}
