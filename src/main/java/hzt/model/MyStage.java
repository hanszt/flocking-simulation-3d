package hzt.model;

import javafx.event.Event;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public final class MyStage extends Stage {

    public void inverseFullScreenProperty(Event e) {
        super.setFullScreen(!super.isFullScreen());
        e.consume();
    }

    public void setFullScreenWhenF11Pressed(KeyEvent keyEvent) {
        if (keyEvent.getCode() == KeyCode.F11) {
            inverseFullScreenProperty(keyEvent);
        }
    }
}
