package hzt.model;

import hzt.model.controls3d.Translation3DKeyFilter;
import javafx.geometry.Point3D;
import javafx.scene.Group;
import javafx.scene.PerspectiveCamera;
import javafx.scene.Scene;
import javafx.scene.input.KeyEvent;
import javafx.util.Duration;

import static hzt.model.AppConstants.*;
import static hzt.model.utils.MovementManager.limit;
import static javafx.scene.input.KeyCode.*;

public class MovableCameraPlatform extends Group {

    private static final double FAR_CLIP = 10_000;
    private static final double NEAR_CLIP = 0.1;
    private static final double FRICTION_FACTOR_CAMERA = 1;

    private final PerspectiveCamera camera = new PerspectiveCamera(true);//fixedEyeAtCameraZero
    private final Translation3DKeyFilter translation3DKeyFilter = new Translation3DKeyFilter(G, J, Y, H, K, I);
    private Point3D velocity;
    private Point3D acceleration;



    public MovableCameraPlatform() {
        super();
        this.velocity = this.acceleration = Point3D.ZERO;
        configureCamera();
        super.getChildren().add(camera);
    }

    private void configureCamera() {
        camera.setNearClip(NEAR_CLIP);
        camera.setFarClip(FAR_CLIP);
    }

    public void update(Duration deltaT, double maxVelocity, double maxAcceleration) {
        acceleration = Point3D.ZERO;
        double correctedMaxAcceleration = maxAcceleration / (deltaT.toSeconds() * ACCELERATION_CORRECTION_FACTOR);
        translation3DKeyFilter.setUserInputSize(correctedMaxAcceleration);
        acceleration = acceleration.add(translation3DKeyFilter.getUserInputAcceleration());
        acceleration = acceleration.add(friction());
        updatePosition(deltaT, maxVelocity, correctedMaxAcceleration);
    }

    public void updatePosition(Duration frameDuration, double maxVelocity, double maxAcceleration) {
        double deltaTSeconds = frameDuration.toSeconds();
        var position = new Point3D(getTranslateX(), getTranslateY(), getTranslateZ());
        acceleration = limit(maxAcceleration, acceleration);
        velocity = velocity.add(acceleration.multiply(deltaTSeconds));
        velocity = limit(maxVelocity, velocity);
        position = position.add(velocity.multiply(deltaTSeconds));
        setTranslateX(position.getX());
        setTranslateY(position.getY());
        setTranslateZ(position.getZ());
    }

    public Point3D friction() {
        Point3D decelerationDir = velocity.multiply(-1);
        return decelerationDir.multiply(FRICTION_FACTOR_CAMERA);
    }

    public void addKeyControls(Scene scene) {
        scene.addEventFilter(KeyEvent.KEY_PRESSED, translation3DKeyFilter.getKeyPressed());
        scene.addEventFilter(KeyEvent.KEY_RELEASED, translation3DKeyFilter.getKeyReleased());
    }

    public PerspectiveCamera getCamera() {
        return camera;
    }

    public Point3D getVelocity() {
        return velocity;
    }

    public Point3D getAcceleration() {
        return acceleration;
    }
}
