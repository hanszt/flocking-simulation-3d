package hzt.model;

import hzt.model.geometry3d.Dimension3D;
import hzt.model.utils.FxUtils;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.geometry.Point3D;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.paint.Color;
import javafx.scene.paint.Material;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Shape3D;
import javafx.scene.transform.Rotate;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import static hzt.model.utils.RandomGenerator.getRandomDouble;
import static hzt.model.utils.StringUtils.toOnlyFirstLetterUpperCase;
import static javafx.scene.shape.DrawMode.FILL;
import static javafx.scene.shape.DrawMode.LINE;

public abstract class Aquarium implements Comparable<Aquarium> {

    public static final Aquarium.Dimension AQUARIUM_DIMENSION3D = new Aquarium.Dimension(300, 200, 300, 150);
    public static final Point3D INIT_AQUARIUM_POSITION = new Point3D(0, 0, 0);
    public static final Rotate INIT_AQUARIUM_ROTATION = new Rotate(0, Rotate.X_AXIS);

    private final Node aquariumShape;
    private final String name;
    private final PhongMaterial material = new PhongMaterial();

    private final BooleanProperty bounceOfWall = new SimpleBooleanProperty();

    protected Aquarium(Node aquariumShape, String name) {
        this.aquariumShape = aquariumShape;
        this.name = name;
        this.aquariumShape.setDisable(true);
        if (aquariumShape instanceof Shape3D shape3D) {
            (shape3D).setMaterial(material);
        } else if (aquariumShape instanceof Group group) {
            setMaterialForChildren(group, material);
        } else {
            throw new IllegalStateException();
        }
        material.setSpecularColor(Color.WHITE);
    }

    public abstract void setAquariumDimension(Dimension dimension);

    public void setTranslate3D(Point3D position) {
        aquariumShape.setTranslateX(position.getX());
        aquariumShape.setTranslateY(position.getY());
        aquariumShape.setTranslateZ(position.getZ());
    }

    public void setRotation(Rotate rotation) {
        aquariumShape.setRotationAxis(rotation.getAxis());
        aquariumShape.setRotate(rotation.getAngle());
    }

    public static class Dimension extends Dimension3D {

        private final double radius;

        public Dimension(double width, double height, double depth, double radius) {
            super(width, height, depth);
            this.radius = radius;
        }

        public double getRadius() {
            return radius;
        }
    }

    public void setTransparentColor(Color color, double opacity) {
        var transparentColor = new Color(color.getRed(), color.getGreen(), color.getBlue(), opacity);
        material.setDiffuseColor(transparentColor);
    }

    private static void setMaterialForChildren(Group aquariumShape, Material material) {
        FxUtils.allDescendents(aquariumShape).stream()
                .filter(Shape3D.class::isInstance)
                .map(Shape3D.class::cast)
                .forEach(s -> s.setMaterial(material));
    }

    public void setDisplayMode(DisplayMode displayMode) {
        if (aquariumShape instanceof Shape3D shape3D) {
            setDisplayMode(shape3D, displayMode);
        } else if (aquariumShape instanceof Parent parent) {
            FxUtils.allDescendents(parent).stream()
                    .filter(Shape3D.class::isInstance)
                    .map(Shape3D.class::cast)
                    .forEach(e -> setDisplayMode(e, displayMode));
        } else {
            throw new IllegalStateException();
        }
    }

    private static void setDisplayMode(Shape3D aquariumShape, DisplayMode displayMode) {
        aquariumShape.setVisible(displayMode != DisplayMode.INVISIBLE);
        if (displayMode == DisplayMode.TRANSPARENT) {
            aquariumShape.setDrawMode(FILL);
        }
        if (displayMode == DisplayMode.WIREFRAME) {
            aquariumShape.setDrawMode(LINE);
        }
    }

    @Override
    public boolean equals(Object o) {
        return this == o || (o instanceof Aquarium that && Objects.equals(name, that.name));
    }

    public static Point3D getRandomPositionInAquarium() {
        final var TWO = 2;
        double maxX = AQUARIUM_DIMENSION3D.getWidth() / TWO;
        double maxY = AQUARIUM_DIMENSION3D.getHeight() / TWO;
        double maxZ = AQUARIUM_DIMENSION3D.getDepth() / TWO;
        double minX = INIT_AQUARIUM_POSITION.getX() - maxX;
        double minY = INIT_AQUARIUM_POSITION.getY() - maxY;
        double minZ = INIT_AQUARIUM_POSITION.getZ() - maxZ;
        var xPos = getRandomDouble(minX, maxX);
        var yPos = getRandomDouble(minY, maxY);
        var zPos = getRandomDouble(minZ, maxZ);
        return new Point3D(xPos, yPos, zPos);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public int compareTo(@NotNull Aquarium o) {
        return name.compareTo(o.name);
    }

    public BooleanProperty bounceOfWallProperty() {
        return bounceOfWall;
    }

    public Node getAquariumShape() {
        return aquariumShape;
    }

    @Override
    public String toString() {
        return name;
    }

    public enum DisplayMode {

        TRANSPARENT,
        WIREFRAME,
        INVISIBLE;

        @Override
        public String toString() {
            return toOnlyFirstLetterUpperCase(name());
        }
    }
}
