package hzt.view;

import hzt.controller.AppManager;
import hzt.model.MyStage;
import javafx.application.Application;
import javafx.stage.Stage;

public class Launcher extends Application {

    public static void main(String[] args) {
        launch();
    }

    @Override
    public void start(Stage stage) {
        new AppManager(new MyStage()).start();
    }

}
