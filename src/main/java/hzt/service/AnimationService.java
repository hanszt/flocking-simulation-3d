package hzt.service;

import hzt.model.Flock;
import hzt.model.MovableCameraPlatform;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.util.Duration;

import static hzt.model.AppConstants.INIT_FRAME_RATE;
import static javafx.animation.Animation.INDEFINITE;

public class AnimationService {

    private static final int CAMERA_BOUNDS_MULTIPLIER = 2;
    private static final Duration INIT_FRAME_DURATION = Duration.seconds(1. / INIT_FRAME_RATE); // s/f

    private final Timeline animationTimeLine = setupTimeLine();
    private Timeline mainTimeline;


    public AnimationService() {
        super();
    }

    public Timeline setupTimeLine() {
        Timeline t = new Timeline();
        t.setCycleCount(INDEFINITE);
        return t;
    }

    public void addLoopsToTimelines(EventHandler<ActionEvent> mainLoopEventHandler,
                                    EventHandler<ActionEvent> animationLoopEventHandler) {
        KeyFrame animationLoopKeyFrame = new KeyFrame(INIT_FRAME_DURATION,
                "animation keyframe", animationLoopEventHandler);
        KeyFrame mainLoopKeyFrame = new KeyFrame(INIT_FRAME_DURATION, "main keyFrame", mainLoopEventHandler);
        animationTimeLine.getKeyFrames().add(animationLoopKeyFrame);
        Task<Integer> mainLoopTask = new Task<>() {
            @Override
            protected Integer call() {
                mainTimeline = setupTimeLine();
                mainTimeline.getKeyFrames().add(mainLoopKeyFrame);
                mainTimeline.play();
                return 0;
            }
        };
        Thread mainTimelineThread = new Thread(mainLoopTask);
        start(mainTimelineThread);
    }

    private void start(Thread mainTimelineThread) {
        animationTimeLine.play();
        mainTimelineThread.start();
    }

    public void runMain(MovableCameraPlatform cameraPlatform, double maxVelocity, double maxAcceleration) {
        double maxCameraVelocity = maxVelocity * CAMERA_BOUNDS_MULTIPLIER;
        double maxCameraAcceleration = maxAcceleration * CAMERA_BOUNDS_MULTIPLIER;
        updateMovableCamera(cameraPlatform, maxCameraVelocity, maxCameraAcceleration);
    }

    public void runAnimation(Flock flock, double maxVelocity, double maxAcceleration, double frictionFactor) {
        Duration frameDuration = animationTimeLine.getCycleDuration();
        flock.forEach(boid3D -> boid3D.update(frameDuration, frictionFactor, maxVelocity, maxAcceleration));
    }

    private void updateMovableCamera(MovableCameraPlatform cameraPlatform, double maxCameraVelocity,
                                     double maxAcceleration) {
        cameraPlatform.update(mainTimeline.getCycleDuration(), maxCameraVelocity, maxAcceleration);
    }

    public void startAnimationTimeline() {
        animationTimeLine.play();
    }

    public void pauseAnimationTimeline() {
        animationTimeLine.pause();
    }


}
