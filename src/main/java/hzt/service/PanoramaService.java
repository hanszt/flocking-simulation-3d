package hzt.service;

import javafx.scene.image.Image;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Stream;

public class PanoramaService implements IPanoramaService {

    private static final String RELATIVE_PANO_IMAGES_RESOURCE_DIR = "/images/panoramas";
    private static final Random RANDOM = new Random();
    private static final Logger LOGGER = LoggerFactory.getLogger(PanoramaService.class);

    private final List<Path> pathsToPanoramas;

    public PanoramaService() {
        pathsToPanoramas = getPathToResourceImages();
    }

    private static List<Path> getPathToResourceImages() {
        final var pathAsString = Optional.ofNullable(PanoramaService.class.getResource(RELATIVE_PANO_IMAGES_RESOURCE_DIR))
                .map(URL::getPath)
                .orElse("");
        try (Stream<Path> walk = Files.walk(new File(pathAsString).toPath())) {
            return walk.filter(Files::isRegularFile).toList();
        } catch (IOException e) {
            LOGGER.error("Could not find {}", RELATIVE_PANO_IMAGES_RESOURCE_DIR, e);
            return Collections.emptyList();
        }
    }

    public Image getRandomPanorama() {
        var path = pathsToPanoramas.get(RANDOM.nextInt(pathsToPanoramas.size()));
        try {
            return new Image(new FileInputStream(path.toString()));
        } catch (FileNotFoundException e) {
            LOGGER.error("could not load image from " + path, e);
        }
        return new Image(InputStream.nullInputStream());
    }
}
