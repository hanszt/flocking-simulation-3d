package hzt.service;

import hzt.model.appearance.Resource;
import hzt.model.utils.StringUtils;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.net.URL;
import java.util.Set;
import java.util.TreeSet;

import static hzt.model.utils.FxUtils.newValue;
import static java.util.Collections.*;

public class ThemeService implements IThemeService {

    private static final String RELATIVE_STYLE_SHEET_RESOURCE_DIR = "/css";

    private static final Logger LOGGER = LoggerFactory.getLogger(ThemeService.class);
    public static final Resource DEFAULT_THEME = new Resource("Light", "style-light.css");

    private final StringProperty styleSheet = new SimpleStringProperty();
    private final ObjectProperty<Resource> currentTheme = new SimpleObjectProperty<>();

    private final Set<Resource> themes;

    public ThemeService() {
        this.themes = scanForThemeStyleSheets();
        currentTheme.addListener(newValue(this::loadResourceStyleSheet));
    }

    private Set<Resource> scanForThemeStyleSheets() {
        Set<Resource> themeSet = new TreeSet<>(Set.of(DEFAULT_THEME, DARK_THEME));
        var url = getClass().getResource(RELATIVE_STYLE_SHEET_RESOURCE_DIR);
        if (url != null) {
            var styleDirectory = new File(url.getFile());
            if (styleDirectory.isDirectory()) {
                String[] fileNames = styleDirectory.list();
                for (String fileName : fileNames != null ? fileNames : new String[0]) {
                    String themeName = extractThemeName(fileName);
                    themeSet.add(new Resource(themeName, fileName));
                }
            }
        } else {
            LOGGER.error("Stylesheet resource folder not found...");
        }
        return themeSet;
    }

    private static String extractThemeName(String fileName) {
        String themeName = fileName
                .replace("style-", "")
                .replace('-', ' ')
                .replace(".css", "");
        return StringUtils.toOnlyFirstLetterUpperCase(themeName);
    }

    private void loadResourceStyleSheet(Resource theme) {
        theme.getPathToResource()
                .map(fileName -> getClass().getResource(RELATIVE_STYLE_SHEET_RESOURCE_DIR + "/" + fileName))
                .ifPresentOrElse(this::setStyleSheet,
                        () -> LOGGER.error("stylesheet of {} could not be loaded...", theme.getName()));
    }

    @Override
    public StringProperty styleSheetProperty() {
        return styleSheet;
    }

    @Override
    public ObjectProperty<Resource> currentThemeProperty() {
        return currentTheme;
    }

    @Override
    public Set<Resource> getThemes() {
        return unmodifiableSet(themes);
    }

    private void setStyleSheet(URL styleSheetUrl) {
        LOGGER.debug("{}", styleSheetUrl);
        this.styleSheet.set(styleSheetUrl.toExternalForm());
    }
}
