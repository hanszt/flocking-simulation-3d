package hzt.service;

import hzt.model.FlockType;
import hzt.model.boid3d.Ball3D;
import hzt.model.boid3d.Boid3D;
import hzt.model.boid3d.Box3D;
import hzt.model.boid3d.Cylinder3D;
import hzt.model.boid3d.Duck3D;
import hzt.model.boid3d.Icosahedron3D;
import hzt.model.boid3d.Pyramid3D;
import hzt.model.boid3d.Scooter3D;
import hzt.model.boid3d.Toroid3D;
import javafx.scene.paint.Color;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

import static hzt.model.utils.RandomGenerator.*;

public class FlockTypes implements Iterable<FlockType> {

    private final Set<FlockType> flockTypeSet;
    private final FlockType initFlockType;

    public FlockTypes() {
        this.initFlockType = new FlockType("Random flock") {
            @Override
            protected Boid3D createBoid(double maxSize, Color color) {
                return getRandomBoid3D(maxSize);
            }
        };
        this.flockTypeSet = new TreeSet<>();
        flockTypeSet.add(initFlockType);
        flockTypeSet.addAll(randomFlockTypes());
        flockTypeSet.addAll(uniformFlockTypes());
        flockTypeSet.add(getBallPanoramaType());
    }

    private static Collection<FlockType> randomFlockTypes() {
        return Set.of(
                new FlockType("Random ball flock") {
                    @Override
                    protected Boid3D createBoid(double maxSize, Color color) {
                        return getRandomBall3D(maxSize);
                    }
                },
                new FlockType("Random box flock") {
                    @Override
                    protected Boid3D createBoid(double maxSize, Color color) {
                        return getRandomBox3D(maxSize);
                    }
                },
                new FlockType("Random cylinder flock") {
                    @Override
                    protected Boid3D createBoid(double maxSize, Color color) {
                        return getRandomCylinder3D(maxSize);
                    }
                },
                new FlockType("Random icosahedron flock") {
                    @Override
                    protected Boid3D createBoid(double maxSize, Color color) {
                        return getRandomIcosahedron(maxSize);
                    }
                },
                new FlockType("Random pyramid flock") {
                    @Override
                    protected Boid3D createBoid(double maxSize, Color color) {
                        return getRandomPyramid(maxSize);
                    }
                },
                new FlockType("Random duck flock") {
                    @Override
                    protected Boid3D createBoid(double maxSize, Color color) {
                        return getRandomDuck(maxSize);
                    }
                },
                new FlockType("Random scooter flock") {
                    @Override
                    protected Boid3D createBoid(double maxSize, Color color) {
                        return getRandomScooter(maxSize);
                    }

                },
                new FlockType("Random toroid flock") {
                    @Override
                    protected Boid3D createBoid(double maxSize, Color color) {
                        return getRandomToroid(maxSize);
                    }
                }
        );
    }

    private static Collection<FlockType> uniformFlockTypes() {
        return Set.of(
                new FlockType("Uniform ball flock") {
                    @Override
                    protected Boid3D createBoid(double maxSize, Color color) {
                        return new Ball3D(maxSize, color);
                    }
                },
                new FlockType("Uniform box flock") {
                    @Override
                    protected Boid3D createBoid(double maxSize, Color color) {
                        return new Box3D(maxSize, color);
                    }
                },
                new FlockType("Uniform cylinder flock") {
                    @Override
                    protected Boid3D createBoid(double maxSize, Color color) {
                        return new Cylinder3D(maxSize, color);
                    }
                },
                new FlockType("Uniform icosahedron flock") {
                    @Override
                    protected Boid3D createBoid(double maxSize, Color color) {
                        return new Icosahedron3D(maxSize, color);
                    }
                },
                new FlockType("Uniform pyramid flock") {
                    @Override
                    protected Boid3D createBoid(double maxSize, Color color) {
                        return new Pyramid3D(maxSize, color);
                    }
                },
                new FlockType("Uniform duck flock") {
                    @Override
                    protected Boid3D createBoid(double maxSize, Color color) {
                        return new Duck3D(maxSize, color);
                    }
                },
                new FlockType("Uniform scooter flock") {
                    @Override
                    protected Boid3D createBoid(double maxSize, Color color) {
                        return new Scooter3D(maxSize, color);
                    }
                },
                new FlockType("Uniform toroid flock") {
                    @Override
                    protected Boid3D createBoid(double maxSize, Color color) {
                        return new Toroid3D(maxSize, color);
                    }
                });

    }

    private static FlockType getBallPanoramaType() {
        return new FlockType("Panorama flock") {

            private final PanoramaService panoramaService = new PanoramaService();

            @Override
            protected Boid3D createBoid(double maxSize, Color color) {
                Ball3D ball3D = new Ball3D(maxSize, color);
                ball3D.getBodyMaterial().setDiffuseMap(panoramaService.getRandomPanorama());
                return ball3D;
            }
        };
    }

    @NotNull
    @Override
    public Iterator<FlockType> iterator() {
        return flockTypeSet.iterator();
    }

    public FlockType getInitFlockType() {
        return initFlockType;
    }
}
