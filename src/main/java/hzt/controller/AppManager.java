package hzt.controller;

import hzt.model.MyStage;
import hzt.model.utils.Loggers;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalTime;
import java.util.Locale;
import java.util.Optional;

import static hzt.model.AppConstants.*;
import static hzt.model.AppConstants.Scene.MAIN_SCENE;
import static java.time.format.DateTimeFormatter.ofPattern;

public class AppManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(AppManager.class);

    private static int instances = 0;
    private final int instance;
    private final Stage stage;
    private final SceneManager sceneManager;

    public AppManager(MyStage stage) {
        this.stage = stage;
        this.sceneManager = new SceneManager(stage);
        this.instance = ++instances;
    }

    public void start() {
        sceneManager.setupScene(MAIN_SCENE);
        Loggers.info(LOGGER, this::startingMessage);
        configureStage(stage);
        Loggers.info(LOGGER, () -> String.format("instance %d started%n", instance));
        stage.show();
    }

    private String startingMessage() {
        return String.format("Starting instance %d of %s at %s...%n",
                instance, TITLE, sceneManager.getCurSceneController()
                        .getStartTimeSim()
                        .format(ofPattern("hh:mm:ss")));
    }

    private void configureStage(Stage stage) {
        stage.setTitle(String.format("%s (%d)", TITLE, instance));
        stage.initStyle(StageStyle.DECORATED);
        stage.setMinWidth(MIN_STAGE_DIMENSION.getWidth());
        stage.setMinHeight(MIN_STAGE_DIMENSION.getHeight());
        stage.setOnCloseRequest(e -> printClosingText());
        Optional.ofNullable(getClass().getResourceAsStream("/images/icon.png"))
                .ifPresent(inputStream -> stage.getIcons().add(new Image(inputStream)));
    }

    private void printClosingText() {
        var startTimeSim = sceneManager.getCurSceneController().getStartTimeSim();
        var stopTimeSim = LocalTime.now();
        var runTimeSim = Duration.millis((stopTimeSim.toNanoOfDay() - startTimeSim.toNanoOfDay()) / 1e6);
        Loggers.info(LOGGER, () -> String.format(Locale.ENGLISH,
                "%s%nAnimation Runtime of instance %d: %.2f seconds%n%s%n",
                CLOSING_MESSAGE, instance, runTimeSim.toSeconds(), DOTTED_LINE));
    }
}
