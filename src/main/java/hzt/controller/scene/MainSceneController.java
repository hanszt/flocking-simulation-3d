package hzt.controller.scene;

import hzt.controller.SceneManager;
import hzt.controller.sub_pane.AppearanceController;
import hzt.controller.sub_pane.LightPositioningController;
import hzt.controller.sub_pane.StatisticsController;
import hzt.model.Aquarium;
import hzt.model.Aquariums;
import hzt.model.Flock;
import hzt.model.FlockType;
import hzt.service.FlockTypes;
import hzt.model.MovableCameraPlatform;
import hzt.model.MyStage;
import hzt.model.boid3d.Boid3D;
import hzt.model.component3d.CoordinateSystem3D;
import hzt.model.component3d.CylinderCoordinateSystem3D;
import hzt.model.utils.Engines;
import hzt.service.AnimationService;
import hzt.service.MouseControlService;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Dimension2D;
import javafx.geometry.Insets;
import javafx.geometry.Point3D;
import javafx.scene.Group;
import javafx.scene.SceneAntialiasing;
import javafx.scene.SubScene;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Slider;
import javafx.scene.control.Tab;
import javafx.scene.control.ToggleButton;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.util.Duration;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.time.LocalTime;
import java.util.EnumSet;

import static hzt.model.AppConstants.*;
import static hzt.model.AppConstants.Scene.MAIN_SCENE;
import static hzt.model.Aquarium.DisplayMode.TRANSPARENT;
import static hzt.model.utils.FxUtils.newValue;
import static java.util.function.Predicate.not;

public class MainSceneController extends SceneController {

    private static final int INIT_MAX_VELOCITY = 50;
    private static final int INIT_MAX_ACCELERATION = 5;
    private static final int INIT_ATTRACTION = 2;
    private static final int INIT_REPEL_FACTOR = 1;
    private static final int INIT_REPEL_DISTANCE_FACTOR = 10;
    private static final int INIT_MAX_BALL_SIZE = 5;
    private static final int INIT_PERCEPTION_RADIUS_FACTOR = 50;
    private static final double INIT_FRICTION = 1;
    private static final double AQUARIUM_OPACITY = .2;
    private static final Dimension2D INIT_ANIMATION_PANE_DIMENSION;
    private static final int DEFAULT_ANIMATION_PANE_SIZE = 640;
    private static final double INIT_ANGLE_X = parsedDoubleAppProp("init_sub_scene_root_angle_x", 30);
    private static final double INIT_ANGLE_Y = parsedDoubleAppProp("init_sub_scene_root_angle_y", 30);
    private static final int INIT_NUMBER_OF_BOIDS3D = parsedIntAppProp("init_number_of_boids", 50);
    private static final int MAX_NUMBER_OF_BOIDS3D = parsedIntAppProp("max_number_of_boids", 100);

    @FXML
    private ToggleButton opacityButton;
    @FXML
    private Tab lightPositioningTab;
    @FXML
    private Tab appearanceTab;
    @FXML
    private Tab statisticsTab;

    @FXML
    private AnchorPane animationPane;
    @FXML
    private ComboBox<Aquarium> aquariumComboBox;
    @FXML
    private ComboBox<FlockType> flockSettingsComboBox;
    @FXML
    private ComboBox<Engines.FlockingSim> physicsEngineComboBox;
    @FXML
    private ChoiceBox<Aquarium.DisplayMode> aquariumDisplayBox;

    @FXML
    private ToggleButton bounceWallsButton;

    @FXML
    private ToggleButton showConnectionsButton;
    @FXML
    private ToggleButton showVelocityVectorButton;
    @FXML
    private ToggleButton showPathSelectedButton;
    @FXML
    private ToggleButton fullScreenButton;
    @FXML
    private ToggleButton showAccelerationVectorButton;
    @FXML
    private ToggleButton showRepelCircleButton;
    @FXML
    private ToggleButton showPerceptionButton;
    @FXML
    private ToggleButton followSelectedBoidButton;
    @FXML
    private ToggleButton showPerceptionSelectedBoidButton;
    @FXML
    private ToggleButton showAxisButton;
    @FXML
    private ColorPicker uniformColorPicker;
    @FXML
    private ColorPicker backgroundColorPicker;
    @FXML
    private ColorPicker selectedColorPicker;
    @FXML
    private ColorPicker aquariumColorPicker;
    @FXML
    private ColorPicker flockLightColorPicker;
    @FXML
    private ColorPicker mainLightColorPicker;
    @FXML
    private Slider numberOfBoidsSlider;

    @FXML
    private Slider maxBoidSizeSlider;
    @FXML
    private Slider perceptionRadiusSlider;
    @FXML
    private Slider attractionSlider;
    @FXML
    private Slider repelFactorSlider;
    @FXML
    private Slider frictionSlider;
    @FXML
    private Slider repelRadiusSlider;
    @FXML
    private Slider accelerationSlider;
    @FXML
    private Slider maxVelocitySlider;
    @FXML

    private Slider boidTailLengthSlider;
    @FXML
    private Slider boidVelocityVectorLengthSlider;
    @FXML
    private Slider boidAccelerationVectorLengthSlider;
    @FXML
    private Slider cameraVelocitySlider;

    @FXML
    private Slider cameraAccelerationSlider;
    @FXML
    private Slider cameraFieldOfViewSlider;

    private final MovableCameraPlatform cameraPlatform = new MovableCameraPlatform();
    private final Engines engines = new Engines();
    private final Group worldRoot = new Group();

    private final MouseControlService mouseControlService = new MouseControlService(worldRoot);
    private final StatisticsController statisticsController = new StatisticsController();
    private final LightPositioningController lightPositioningController = new LightPositioningController(this);
    private final AnimationService animationService = new AnimationService();
    private final Flock flock;
    private final Group subSceneRoot;

    public MainSceneController(SceneManager sceneManager) throws IOException {
        super(MAIN_SCENE.getFxmlFileName(), sceneManager);
        this.flock = new Flock(scene);
        this.subSceneRoot = new Group(worldRoot);
        var subScene3D = new SubScene(subSceneRoot, 0, 0, true, SceneAntialiasing.BALANCED);
        configure3DSubScene(subScene3D);
        this.statisticsTab.setContent(statisticsController.getRoot());
        this.animationPane.getChildren().add(subScene3D);
    }

    @Override
    public void setup() {
        bindFullScreenButtonToFullScreen(fullScreenButton, sceneManager.getStage());
        configureAnimationPane(animationPane);
        configureBoxes();
        reset();
        recenter();
        bindFlockPropertiesToControlProperties(flock);
        configureFlock(flock);
        configureCameraPlatform(cameraPlatform);
        configureColorPickers();
        configureSliders();
        configureAquarium(aquariumComboBox.getValue());
        configureLightPositioningController();
        configureAppearanceController(lightPositioningController);
        final CoordinateSystem3D inertialCoordinateSystem = buildInertialCoordinateSystem();
        worldRoot.getChildren().addAll(flock, inertialCoordinateSystem, aquariumComboBox.getValue().getAquariumShape());
        animationService.addLoopsToTimelines(this::mainLoop, this::animationLoop);
    }

    @NotNull
    private CoordinateSystem3D buildInertialCoordinateSystem() {
        final CoordinateSystem3D inertialCoordinateSystem = new CylinderCoordinateSystem3D();
        inertialCoordinateSystem.visibleProperty().bind(showAxisButton.selectedProperty());
        return inertialCoordinateSystem;
    }

    private void configureLightPositioningController() {
        lightPositioningController.setup();
        lightPositioningController.configureLightColorPickers(mainLightColorPicker, flockLightColorPicker);
        lightPositioningController.getMovableFlockLight().coordinateSystem3DVisibleProperty()
                .bind(showAxisButton.selectedProperty());
        subSceneRoot.getChildren().add(lightPositioningController.getMainLight());
        worldRoot.getChildren().add(lightPositioningController.getMovableFlockLight());
        lightPositioningTab.setContent(lightPositioningController.getRoot());
    }

    private void configureAppearanceController(LightPositioningController lightPositioningController) {
        try {
            var appearanceController = new AppearanceController(this);
            appearanceController.setup(lightPositioningController.getMainLight(),
                    lightPositioningController.getMovableFlockLight());
            appearanceController.showCoordinateSystemsButtonSelectedProperty()
                    .bindBidirectional(showAxisButton.selectedProperty());
            appearanceController.stageOpacityButtonSelectedProperty()
                    .bindBidirectional(opacityButton.selectedProperty());
            this.appearanceTab.setContent(appearanceController.getRoot());
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
    private void animationLoop(ActionEvent loop) {
        double maxVelocity = maxVelocitySlider.getValue();
        double maxAcceleration = accelerationSlider.getValue();
        double friction = frictionSlider.getValue();
        animationService.runAnimation(flock, maxVelocity, maxAcceleration, friction);
    }

    private void mainLoop(ActionEvent loop) {
        double maxVelocity = maxVelocitySlider.getValue();
        double maxAcceleration = accelerationSlider.getValue();
        double frictionFactor = frictionSlider.getValue();
        Boid3D selected = flock.getSelectedBoid();
        var runTimeSim = Duration.millis((LocalTime.now().toNanoOfDay() - startTimeSim.toNanoOfDay()) / 1e6);

        statisticsController.showGlobalStatistics(frictionFactor, flock.size(), runTimeSim, mouseControlService);
        statisticsController.showStatisticsAboutSelectedObject(selected);
        statisticsController.showStatisticsAboutCameraPlatform(cameraPlatform);
        animationService.runMain(cameraPlatform, maxVelocity, maxAcceleration);
    }

    static {
        int animationPaneWidthProp = parsedIntAppProp("init_animation_pane_width", DEFAULT_ANIMATION_PANE_SIZE);
        int animationPaneHeightProp = parsedIntAppProp("init_animation_pane_height", DEFAULT_ANIMATION_PANE_SIZE);
        INIT_ANIMATION_PANE_DIMENSION = new Dimension2D(animationPaneWidthProp, animationPaneHeightProp);
    }

    private static void configureAnimationPane(AnchorPane animationPane) {
        animationPane.setPrefSize(INIT_ANIMATION_PANE_DIMENSION.getWidth(), INIT_ANIMATION_PANE_DIMENSION.getHeight());
        animationPane.setBackground(new Background(new BackgroundFill(INIT_BG_COLOR, CornerRadii.EMPTY, Insets.EMPTY)));
    }

    private void configureCameraPlatform(MovableCameraPlatform cameraPlatform) {
        cameraPlatform.addKeyControls(scene);
        cameraPlatform.setTranslateZ(INIT_CAMERA_TRANSLATE_Z);
    }

    private static void bindFullScreenButtonToFullScreen(ToggleButton fullScreenButton, MyStage stage) {
        stage.fullScreenProperty().addListener(newValue(fullScreenButton::setSelected));
        stage.addEventFilter(KeyEvent.KEY_TYPED, stage::setFullScreenWhenF11Pressed);
    }

    private void configureSliders() {
        numberOfBoidsSlider.valueProperty().addListener(newValue(flock::setFlockSize));

        perceptionRadiusSlider.valueProperty().addListener(newValue(flock::setPerceptionRadiusForEachBoid));
        repelRadiusSlider.valueProperty().addListener(newValue(flock::setRepelRadiusRatio));

        engines.pullFactorProperty().bind(attractionSlider.valueProperty());
        engines.repelFactorProperty().bind(repelFactorSlider.valueProperty());

        cameraPlatform.getCamera().fieldOfViewProperty().bind(cameraFieldOfViewSlider.valueProperty());

        cameraVelocitySlider.valueProperty().bindBidirectional(maxVelocitySlider.valueProperty());
        cameraAccelerationSlider.valueProperty().bindBidirectional(accelerationSlider.valueProperty());

        boidTailLengthSlider.setDisable(true);
        boidVelocityVectorLengthSlider.setDisable(true);
        boidAccelerationVectorLengthSlider.setDisable(true);
    }

    private void bindFlockPropertiesToControlProperties(Flock flock) {
        var properties = flock.getFlockProperties();
        properties.maxBallSizeProperty().bind(maxBoidSizeSlider.valueProperty());
        properties.perceptionRadiusRatioProperty().bind(perceptionRadiusSlider.valueProperty());
        properties.repelRadiusRatioProperty().bind(repelRadiusSlider.valueProperty());
        properties.velocityVectorsVisibleProperty().bind(showVelocityVectorButton.selectedProperty());
        properties.accelerationVectorsVisibleProperty().bind(showAccelerationVectorButton.selectedProperty());
        properties.perceptionSpheresVisibleProperty().bind(showPerceptionButton.selectedProperty());
        properties.repelSphereVisibleProperty().bind(showRepelCircleButton.selectedProperty());
        properties.perceptionVisibleSelectedProperty().bind(showPerceptionSelectedBoidButton.selectedProperty());
        properties.showConnectionsProperty().bind(showConnectionsButton.selectedProperty());
        properties.boidColorProperty().bind(uniformColorPicker.valueProperty());
    }

    private void configureColorPickers() {
        backgroundColorPicker.setValue(INIT_BG_COLOR);
        uniformColorPicker.setValue(INIT_OBJECT_UNIFORM_COLOR);
        selectedColorPicker.setValue(INIT_SELECTED_BOID_COLOR);
        aquariumColorPicker.setValue(INIT_AQUARIUM_COLOR);
        uniformColorPicker.setDisable(flockSettingsComboBox.getValue().isRandom());
    }

    private void configureBoxes() {
        final var aquariums = new Aquariums();
        aquariums.forEach(aquarium -> aquariumComboBox.getItems().add(aquarium));
        var curAquarium = aquariums.getRandomAquarium();
        aquariumComboBox.setValue(curAquarium);
        aquariumComboBox.valueProperty().addListener(this::aquariumComboboxAction);
        configureFlockTypeComboBox();
        EnumSet.allOf(Aquarium.DisplayMode.class).forEach(mode -> aquariumDisplayBox.getItems().add(mode));

        aquariumDisplayBox.setValue(TRANSPARENT);
        physicsEngineComboBox.getItems().addAll(engines.getType1(), engines.getType2(), engines.getType3());
        physicsEngineComboBox.setValue(engines.getType1());
    }

    private void configureFlockTypeComboBox() {
        final var flockTypes = new FlockTypes();
        flockTypes.forEach(type -> flockSettingsComboBox.getItems().add(type));
        flockSettingsComboBox.setValue(flockTypes.getInitFlockType());
    }

    private void configureAquarium(Aquarium aquarium) {
        aquarium.bounceOfWallProperty().bind(bounceWallsButton.selectedProperty());
        aquarium.setAquariumDimension(Aquarium.AQUARIUM_DIMENSION3D);
        aquarium.setTransparentColor(aquariumColorPicker.getValue(), AQUARIUM_OPACITY);
        aquarium.setTranslate3D(Aquarium.INIT_AQUARIUM_POSITION);
        aquarium.setRotation(Aquarium.INIT_AQUARIUM_ROTATION);
        aquarium.setDisplayMode(aquariumDisplayBox.getValue());
    }

    private void reset() {
        resetSliders();
        configureButtons();
    }

    private void resetSliders() {
        maxBoidSizeSlider.setValue(INIT_MAX_BALL_SIZE);
        numberOfBoidsSlider.setValue(INIT_NUMBER_OF_BOIDS3D);
        numberOfBoidsSlider.setMax(MAX_NUMBER_OF_BOIDS3D);
        accelerationSlider.setValue(INIT_MAX_ACCELERATION);
        attractionSlider.setValue(INIT_ATTRACTION);
        repelRadiusSlider.setValue(INIT_REPEL_DISTANCE_FACTOR);
        repelFactorSlider.setValue(INIT_REPEL_FACTOR);
        frictionSlider.setValue(INIT_FRICTION);
        perceptionRadiusSlider.setValue(INIT_PERCEPTION_RADIUS_FACTOR);
        maxVelocitySlider.setValue(INIT_MAX_VELOCITY);
        cameraFieldOfViewSlider.setValue(INIT_FIELD_OF_VIEW_CAMERA);
    }

    private void configureButtons() {
        followSelectedBoidButton.setSelected(false);
        showConnectionsButton.setSelected(false);
        showPathSelectedButton.setSelected(false);
        showVelocityVectorButton.setSelected(false);
        showAccelerationVectorButton.setSelected(false);
        showPerceptionButton.setSelected(false);
        showPerceptionSelectedBoidButton.setSelected(false);
        bounceWallsButton.setSelected(true);
        showAxisButton.setSelected(false);
    }

    private void configureFlock(Flock flock) {
        flock.setFlockType(flockSettingsComboBox.getValue());
        flock.setFlockSize(numberOfBoidsSlider.valueProperty().intValue());
        flock.setFlockingSim(engines.getType1());
        flock.setSelectedBoid(flock.getRandomBoidFromFlock());
    }

    private void configure3DSubScene(SubScene subScene3D) {
        subScene3D.widthProperty().bind(animationPane.widthProperty());
        subScene3D.heightProperty().bind(animationPane.heightProperty());
        subScene3D.setCamera(cameraPlatform.getCamera());
        mouseControlService.initMouseControl(animationPane);
    }

    @FXML
    private void physicsEngineComboBoxAction(ActionEvent event) {
        flock.setFlockingSim((Engines.FlockingSim) ((ComboBox<?>) event.getSource()).getValue());
    }

    @FXML
    private void flockTypeComboboxAction() {
        flock.setFlockSize(0);
        configureFlock(flock);
        uniformColorPicker.setDisable(flockSettingsComboBox.getValue().isRandom());
    }

    private void aquariumComboboxAction(ObservableValue<? extends Aquarium> o, Aquarium cur, Aquarium n) {
        worldRoot.getChildren().remove(cur.getAquariumShape());
        configureAquarium(n);
        worldRoot.getChildren().add(n.getAquariumShape());
    }

    @FXML
    private void resetButtonAction() {
        reset();
        configureFlock(flock);
    }

    @FXML
    private void recenter() {
        cameraPlatform.setTranslateX(0);
        cameraPlatform.setTranslateY(0);
        cameraPlatform.setTranslateZ(INIT_CAMERA_TRANSLATE_Z);
        mouseControlService.setOrientation(INIT_ANGLE_X, INIT_ANGLE_Y);
        mouseControlService.setTargetTranslation(Point3D.ZERO);
        lightPositioningController.recenter();
    }

    public void transparentAction(ActionEvent actionEvent) {
        transparentButtonAction(actionEvent);
    }

    @FXML
    private void transparentButtonAction(ActionEvent actionEvent) {
        boolean transparent = ((ToggleButton) actionEvent.getSource()).isSelected();
        final var DEFAULT_VAL = .8D;
        var opacity = parsedDoubleAppProp("stage_opacity", DEFAULT_VAL);
        sceneManager.getStage().setOpacity(transparent ? opacity : 1);
    }

    @FXML
    private void pauseSimButtonAction(ActionEvent actionEvent) {
        if (((ToggleButton) actionEvent.getSource()).isSelected()) {
            animationService.pauseAnimationTimeline();
        } else {
            animationService.startAnimationTimeline();
        }
    }

    @FXML
    private void showRepelSphereButtonAction(ActionEvent event) {
        boolean visible = ((ToggleButton) event.getSource()).isSelected();
        flock.forEach(boid -> boid.getRepelSphere().setVisible(visible));
    }

    @FXML
    private void showPerceptionSphereButtonAction(ActionEvent event) {
        boolean visible = ((ToggleButton) event.getSource()).isSelected();
        flock.forEach(boid -> boid.getPerceptionSphere().setVisible(visible));
        showPerceptionSelectedBoidButton.setSelected(visible);
    }

    @FXML
    public void followSelectedBoidButtonAction() {
        throw new UnsupportedOperationException();
    }

    @FXML
    private void showPerceptionSelectedBoidButtonAction() {
        flock.updateSelectedBoidComponentsVisibility();
    }

    @FXML
    private void fullScreenButtonAction(ActionEvent actionEvent) {
        sceneManager.getStage().setFullScreen(((ToggleButton) actionEvent.getSource()).isSelected());
    }

    @FXML
    public void showPathSelectedBoidButtonAction() {
        throw new UnsupportedOperationException();
    }

    @FXML
    private void aquariumDisplayChoiceBoxAction() {
        aquariumComboBox.getValue().setDisplayMode(aquariumDisplayBox.getValue());
    }

    @FXML
    private void showVelocitiesButtonAction(ActionEvent event) {
        boolean visible = ((ToggleButton) event.getSource()).isSelected();
        flock.forEach(boid -> boid.getVisibleVelocityVector().setVisible(visible));
    }

    @FXML
    private void showAccelerationsButtonAction(ActionEvent event) {
        boolean visible = ((ToggleButton) event.getSource()).isSelected();
        flock.forEach(boid -> boid.getVisibleAccelerationVector().setVisible(visible));
    }

    @FXML
    void backgroundColorPickerAction(ActionEvent event) {
        var backgroundColor = ((ColorPicker) event.getSource()).getValue();
        animationPane.setBackground(
                new Background(new BackgroundFill(backgroundColor, CornerRadii.EMPTY, Insets.EMPTY)));
    }

    @FXML
    void setSelectedColor(ActionEvent event) {
        var color = ((ColorPicker) event.getSource()).getValue();
        flock.setSelectedBoidColor(color);
        flock.getSelectedBoid().updateColor(color);
    }

    @FXML
    private void setAquariumColor(ActionEvent event) {
        var color = ((ColorPicker) event.getSource()).getValue();
        aquariumComboBox.getValue().setTransparentColor(color, AQUARIUM_OPACITY);
    }

    @FXML
    void setUniformColor(ActionEvent event) {
        var color = ((ColorPicker) event.getSource()).getValue();
        flock.stream()
                .filter(not(flock::equalsSelectedBoid))
                .forEach(boid3D -> boid3D.updateColor(color));
    }

    protected SceneController getController() {
        return this;
    }

    public AnchorPane getAnimationPane() {
        return animationPane;
    }

    public ColorPicker getBackgroundColorPicker() {
        return backgroundColorPicker;
    }
}
