package hzt.controller.sub_pane;

import hzt.controller.FXMLController;
import hzt.controller.scene.MainSceneController;
import hzt.controller.scene.SceneController;
import hzt.model.VisiblePointLight;
import hzt.model.appearance.Resource;
import hzt.service.BackgroundService;
import hzt.service.IBackgroundService;
import hzt.service.IThemeService;
import hzt.service.ThemeService;
import javafx.beans.property.BooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.CornerRadii;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;

import static hzt.model.AppConstants.INIT_BG_COLOR;
import static hzt.model.utils.FxUtils.newValue;

public class AppearanceController extends FXMLController {

    @FXML
    private ComboBox<Resource> backgroundCombobox;
    @FXML
    private ComboBox<Resource> themeCombobox;
    @FXML
    private ToggleButton showCoordinateSystemsButton;
    @FXML
    private ToggleButton opacityStageButton;
    @FXML
    private ToggleButton fullScreenButton;
    @FXML
    private ToggleButton pointLightVisibleButton;
    @FXML
    private ColorPicker backgroundColorPicker;

    private final IThemeService themeService = new ThemeService();
    private final MainSceneController mainSceneController;

    public AppearanceController(MainSceneController mainSceneController) throws IOException {
        super("appearancePane.fxml");
        this.mainSceneController = mainSceneController;
    }

    public void setup(VisiblePointLight mainVisiblePointLight, VisiblePointLight eggVisiblePointLight) {
        configureComboBoxes();
        configureColorPickers();
        configureStageControlButtons();
        eggVisiblePointLight.pointLightVisibleProperty().bind(pointLightVisibleButton.selectedProperty());
        mainVisiblePointLight.pointLightVisibleProperty().bind(pointLightVisibleButton.selectedProperty());
    }

    private void configureColorPickers() {
        backgroundColorPicker.setValue(INIT_BG_COLOR);
        mainSceneController.getBackgroundColorPicker().valueProperty()
                .bindBidirectional(backgroundColorPicker.valueProperty());
    }

    public void configureStageControlButtons() {
        var stage = mainSceneController.getSceneManager().getStage();
        fullScreenButton.setOnAction(stage::inverseFullScreenProperty);
        stage.fullScreenProperty().addListener(newValue(fullScreenButton::setSelected));
        stage.addEventFilter(KeyEvent.KEY_PRESSED, stage::setFullScreenWhenF11Pressed);
        opacityStageButton.setOnAction(mainSceneController::transparentAction);
    }

    private void configureComboBoxes() {
        final var items = themeCombobox.getItems();
        themeService.getThemes().forEach(items::add);
        themeService.currentThemeProperty().bind(themeCombobox.valueProperty());
        themeService.styleSheetProperty().addListener(this::changeStyle);
        themeCombobox.setValue(IThemeService.DEFAULT_THEME);

        final IBackgroundService backgroundService = new BackgroundService();
        backgroundService.getResources().forEach(r -> backgroundCombobox.getItems().add(r));
        backgroundCombobox.setValue(BackgroundService.NO_PICTURE);
    }

    public void changeStyle(ObservableValue<? extends String> o, String c, String newVal) {
        var sceneManager = mainSceneController.getSceneManager();
        Collection<SceneController> sceneControllers = sceneManager.getSceneControllerMap().values();
        for (SceneController sceneController : sceneControllers) {
            ObservableList<String> styleSheets = sceneController.getScene().getStylesheets();
            styleSheets.removeIf(filter -> !styleSheets.isEmpty());
            if (newVal != null) {
                styleSheets.add(newVal);
            }
        }
    }

    @FXML
    private void backgroundComboBoxAction() {
        backgroundCombobox.getValue().getPathToResource()
                .map(AppearanceController.class::getResourceAsStream)
                .ifPresentOrElse(this::setBackground, this::backgroundColorPickerAction);
    }

    private void setBackground(InputStream inputStream) {
        var image = new Image(inputStream);
        AnchorPane animationPane = mainSceneController.getAnimationPane();
        animationPane.setBackground(background(image, animationPane));
    }

    @NotNull
    private static Background background(Image image, AnchorPane animationPane) {
        return new Background(new BackgroundImage(image,
                BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.CENTER,
                new BackgroundSize(animationPane.getWidth(), animationPane.getHeight(),
                        false, false, false, true)));
    }

    @FXML
    private void backgroundColorPickerAction() {
        mainSceneController.getAnimationPane().setBackground(new Background(
                new BackgroundFill(backgroundColorPicker.getValue(), CornerRadii.EMPTY, Insets.EMPTY)));
        backgroundCombobox.setValue(BackgroundService.NO_PICTURE);
    }

    public BooleanProperty showCoordinateSystemsButtonSelectedProperty() {
        return showCoordinateSystemsButton.selectedProperty();
    }

    public BooleanProperty stageOpacityButtonSelectedProperty() {
        return opacityStageButton.selectedProperty();
    }

    @Override
    protected FXMLController getController() {
        return this;
    }
}
