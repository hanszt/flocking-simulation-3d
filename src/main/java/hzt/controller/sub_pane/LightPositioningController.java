package hzt.controller.sub_pane;

import hzt.controller.FXMLController;
import hzt.controller.scene.MainSceneController;
import hzt.model.AppConstants;
import hzt.model.VisiblePointLight;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Slider;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import javafx.scene.transform.Rotate;
import javafx.scene.transform.Translate;
import javafx.util.Duration;

import java.io.IOException;

public class LightPositioningController extends FXMLController {

    private static final String ROTATE_LIGHT_Y_ON_OFF = AppConstants.getProperty("rotate_light_y_axis_on_off", "l");
    private static final int INIT_LIGHT_PIVOT_Z = 300;
    private static final int INIT_MAIN_LIGHT_X = -500;
    private static final int INIT_MAIN_LIGHT_Y = -50;

    @FXML
    private Slider lightXAxisRotationSlider;
    @FXML
    private Slider lightYAxisRotationSlider;
    @FXML
    private Slider lightPivotZSlider;
    @FXML
    public Slider translateXSlider;
    @FXML
    public Slider translateYSlider;
    @FXML
    public Slider translateZSlider;


    private final VisiblePointLight mainLight = buildMainLight();
    private final VisiblePointLight movableFlockLight = new VisiblePointLight();

    private final MainSceneController mainSceneController;

    public LightPositioningController(MainSceneController mainSceneController) throws IOException {
        super("lightPositioningPane.fxml");
        this.mainSceneController = mainSceneController;
        recenter();
    }

    private static VisiblePointLight buildMainLight() {
        var pointLight = new VisiblePointLight();
        pointLight.setTranslateZ(INIT_MAIN_LIGHT_X);
        pointLight.setTranslateY(INIT_MAIN_LIGHT_Y);
        return pointLight;
    }

    public void setup() {
        final var rotateX = new Rotate(0, Rotate.X_AXIS);
        final var rotateY = new Rotate(0, Rotate.Y_AXIS);
        final var pivot = new Translate();
        final var translate = new Translate();
        movableFlockLight.bindPivotSphereToPivot(pivot);
        movableFlockLight.getTransforms().addAll(translate, rotateY, rotateX, pivot);

        rotateX.angleProperty().bindBidirectional(lightXAxisRotationSlider.valueProperty());
        rotateY.angleProperty().bindBidirectional(lightYAxisRotationSlider.valueProperty());
        pivot.zProperty().bindBidirectional(lightPivotZSlider.valueProperty());

        translate.xProperty().bindBidirectional(translateXSlider.valueProperty());
        translate.yProperty().bindBidirectional(translateYSlider.valueProperty());
        translate.zProperty().bindBidirectional(translateZSlider.valueProperty());
        animateYAxisLightRotation();
    }

    private void animateYAxisLightRotation() {
        final var DURATION = 4;
        final var HALF_CIRCLE = 180;
        var timeline = new Timeline(
                new KeyFrame(Duration.ZERO, new KeyValue(lightYAxisRotationSlider.valueProperty(), -HALF_CIRCLE)),
                new KeyFrame(Duration.seconds(DURATION),
                        new KeyValue(lightYAxisRotationSlider.valueProperty(), HALF_CIRCLE)));
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.playFromStart();

        var scene = mainSceneController.getScene();
        scene.addEventFilter(KeyEvent.KEY_TYPED, e -> rotateLightWhenPPressed(e, timeline));
        lightYAxisRotationSlider.setOnMousePressed(e -> timeline.pause());
    }

    private static void rotateLightWhenPPressed(KeyEvent e, Animation animation) {
        if (e.getCharacter().equals(ROTATE_LIGHT_Y_ON_OFF)) {
            if (animation.getStatus() == Animation.Status.RUNNING) {
                animation.pause();
            } else {
                animation.play();
            }
        }
    }

    public final void recenter() {
        translateXSlider.setValue(0);
        translateYSlider.setValue(0);
        translateZSlider.setValue(0);

        lightXAxisRotationSlider.setValue(0);
        lightYAxisRotationSlider.setValue(0);
        lightPivotZSlider.setValue(INIT_LIGHT_PIVOT_Z);
    }

    public void configureLightColorPickers(ColorPicker mainLightColorPicker, ColorPicker flockLightColorPicker) {
        mainLight.pointLightColorProperty().bind(mainLightColorPicker.valueProperty());
        movableFlockLight.pointLightColorProperty().bind(flockLightColorPicker.valueProperty());
        mainLightColorPicker.setValue(Color.WHITE);
        flockLightColorPicker.setValue(Color.WHITE);
    }

    public VisiblePointLight getMainLight() {
        return mainLight;
    }

    public VisiblePointLight getMovableFlockLight() {
        return movableFlockLight;
    }

    @Override
    protected FXMLController getController() {
        return this;
    }
}
