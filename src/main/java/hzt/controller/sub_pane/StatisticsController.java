package hzt.controller.sub_pane;

import hzt.controller.FXMLController;
import hzt.model.MovableCameraPlatform;
import hzt.model.boid3d.Boid3D;
import hzt.service.MouseControlService;
import hzt.service.StatisticsService;
import javafx.fxml.FXML;
import javafx.geometry.Point3D;
import javafx.scene.PerspectiveCamera;
import javafx.scene.control.Label;
import javafx.util.Duration;

import java.io.IOException;

import static java.lang.String.format;

public class StatisticsController extends FXMLController {

    private static final String TWO_DEC_DOUBLE = "%-4.2f";
    private static final String DEGREES = " degrees";
    @FXML
    private Label positionXLabel;
    @FXML
    private Label positionYLabel;
    @FXML
    private Label positionZLabel;
    @FXML
    private Label rotationXLabel;
    @FXML
    private Label rotationYLabel;

    @FXML
    private Label frictionStatsLabel;
    @FXML
    private Label frameRateStatsLabel;
    @FXML
    private Label numberOfBoidsLabel;
    @FXML
    private Label runTimeLabel;
    @FXML
    private Label boidNameLabel;
    @FXML
    private Label positionXSelectedBoidLabel;
    @FXML
    private Label positionYSelectedBoidLabel;
    @FXML
    private Label positionZSelectedBoidLabel;
    @FXML
    private Label velocitySelectedBoidLabel;
    @FXML
    private Label accelerationSelectedBoidLabel;
    @FXML
    private Label boidSizeLabel;
    @FXML
    private Label nrOfBoidsInPerceptionRadiusLabel;
    @FXML
    private Label positionCameraXLabel;
    @FXML
    private Label positionCameraYLabel;
    @FXML
    private Label positionCameraZLabel;
    @FXML
    private Label rotationCameraXLabel;
    @FXML
    private Label rotationCameraYLabel;
    @FXML
    private Label rotationCameraZLabel;
    @FXML
    private Label velocityCameraLabel;
    @FXML
    private Label accelerationCameraLabel;
    @FXML
    private Label fieldOfViewCameraLabel;

    private final StatisticsService statisticsService = new StatisticsService();

    public StatisticsController() throws IOException {
        super("statisticsPane.fxml");
    }

    public void showStatisticsAboutSelectedObject(Boid3D selected) {
        if (selected != null) {
            Point3D centerPos = selected.getBodyTranslate();
            boidNameLabel.setText(format("%s", selected.getName()));
            positionXSelectedBoidLabel.setText(format(TWO_DEC_DOUBLE + " p", centerPos.getX()));
            positionYSelectedBoidLabel.setText(format(TWO_DEC_DOUBLE + " p", centerPos.getY()));
            positionZSelectedBoidLabel.setText(format(TWO_DEC_DOUBLE + " p", centerPos.getZ()));
            velocitySelectedBoidLabel.setText(format(TWO_DEC_DOUBLE + " p/s", selected.getVelocity().magnitude()));
            accelerationSelectedBoidLabel.setText(format(TWO_DEC_DOUBLE + " p/s^2",
                    selected.getAcceleration().magnitude()));
            nrOfBoidsInPerceptionRadiusLabel.setText(format("%-3d", selected.getPerceptionRadiusMap().size()));
            final var TWO = 2;
            boidSizeLabel.setText(format(TWO_DEC_DOUBLE + " p", selected.getDistanceFromCenterToOuterEdge() * TWO));
        }
    }

    public void showGlobalStatistics(double friction, int nrOfBoids, Duration runTimeSim,
                                     MouseControlService mouseControlService) {
        frictionStatsLabel.setText(format(TWO_DEC_DOUBLE, friction));
        frameRateStatsLabel.setText(format(TWO_DEC_DOUBLE + " f/s", statisticsService
                .getSimpleFrameRateMeter().getFrameRate()));
        numberOfBoidsLabel.setText(format("%-3d", nrOfBoids));
        runTimeLabel.setText(format("%-4.3f seconds", runTimeSim.toSeconds()));

        positionXLabel.setText(format(TWO_DEC_DOUBLE, mouseControlService.getTargetTranslateX()));
        positionYLabel.setText(format(TWO_DEC_DOUBLE, mouseControlService.getTargetTranslateY()));
        positionZLabel.setText(format(TWO_DEC_DOUBLE, mouseControlService.getTargetTranslateZ()));

        rotationXLabel.setText(format(TWO_DEC_DOUBLE + " deg", mouseControlService.getTargetAngleX()));
        rotationYLabel.setText(format(TWO_DEC_DOUBLE + " deg", mouseControlService.getTargetAngleY()));
    }

    public void showStatisticsAboutCameraPlatform(MovableCameraPlatform platform) {
        PerspectiveCamera camera = platform.getCamera();

        positionCameraXLabel.setText(format(TWO_DEC_DOUBLE + " p", platform.getTranslateX()));
        positionCameraYLabel.setText(format(TWO_DEC_DOUBLE + " p", platform.getTranslateY()));
        positionCameraZLabel.setText(format(TWO_DEC_DOUBLE + " p", platform.getTranslateZ()));

        rotationCameraXLabel.setText(format(TWO_DEC_DOUBLE + DEGREES, 0.));
        rotationCameraYLabel.setText(format(TWO_DEC_DOUBLE + DEGREES, 0.));
        rotationCameraZLabel.setText(format(TWO_DEC_DOUBLE + DEGREES, 0.));

        velocityCameraLabel.setText(format(TWO_DEC_DOUBLE + " p/s", platform.getVelocity().magnitude()));
        accelerationCameraLabel.setText(format(TWO_DEC_DOUBLE + " p/s^2", platform.getAcceleration().magnitude()));

        fieldOfViewCameraLabel.setText(format(TWO_DEC_DOUBLE + DEGREES, camera.getFieldOfView()));
    }

    public StatisticsController getController() {
        return this;
    }
}
