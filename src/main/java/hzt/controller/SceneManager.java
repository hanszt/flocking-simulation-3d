package hzt.controller;

import hzt.controller.scene.AboutController;
import hzt.controller.scene.SceneController;
import hzt.controller.scene.MainSceneController;
import hzt.model.AppConstants;
import hzt.model.MyStage;
import hzt.model.utils.Loggers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.EnumMap;
import java.util.Map;

import static hzt.model.AppConstants.Scene;
import static hzt.model.AppConstants.Scene.ABOUT_SCENE;
import static hzt.model.AppConstants.Scene.MAIN_SCENE;

public class SceneManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(SceneManager.class);

    private final MyStage stage;
    private final Map<Scene, SceneController> sceneControllerMap;
    private SceneController curSceneController;

    public SceneManager(MyStage stage) {
        this.stage = stage;
        this.sceneControllerMap = new EnumMap<>(AppConstants.Scene.class);
        loadFrontend();
    }

    private void loadFrontend() {
        try {
            sceneControllerMap.put(MAIN_SCENE, new MainSceneController(this));
            sceneControllerMap.put(ABOUT_SCENE, new AboutController(this));
        } catch (IOException e) {
            LOGGER.error("Scene could not be set...", e);
        }
    }

    public void setupScene(AppConstants.Scene scene) {
        curSceneController = sceneControllerMap.get(scene);
        stage.setScene(curSceneController.getScene());
        if (!curSceneController.isSetup()) {
            Loggers.info(LOGGER, () -> "setting up " + scene.getEnglishDescription() + "...");
            curSceneController.setup();
        }
    }

    public MyStage getStage() {
        return stage;
    }

    public SceneController getCurSceneController() {
        return curSceneController;
    }

    public Map<Scene, SceneController> getSceneControllerMap() {
        return sceneControllerMap;
    }
}
