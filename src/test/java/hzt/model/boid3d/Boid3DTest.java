package hzt.model.boid3d;

import javafx.scene.paint.Color;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Boid3DTest {

    @Test
    void testDefaultMassSphereBoid() {
        final var ball3D = new Ball3D(Color.WHITE);
        assertEquals(799.9999999999998, ball3D.getMass());
    }

    @Test
    void testDefaultMassBoxBoid() {
        final var box3D = new Box3D(Color.WHITE);
        assertEquals(800.0, box3D.getMass());
    }
}
